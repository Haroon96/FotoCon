[33mcommit 7f7d7c26fd53f15cb50906ac1faaaaaaf1736a72[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Mon Apr 24 01:45:48 2017 +0500

    Several changes

[33mcommit a25adccff01ee1bd516c6a06a920d376fd0be191[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Tue Apr 18 00:08:24 2017 +0500

    Added fragments

[33mcommit 8738545f74a445e8e5f81b630d75b1df98db42cf[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Tue Apr 18 00:07:13 2017 +0500

    Code cleanup

[33mcommit f5a2d4f229126b0d085b326fcb1292aad96f6e5c[m
Merge: 601ff2e ad9ef2b
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Tue Apr 18 00:05:20 2017 +0500

    Merge branch 'master' of https://gitlab.com/Haroon23/FotoCon

[33mcommit 601ff2e7c1e68b37964f872506c577d836bf092b[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Tue Apr 18 00:04:36 2017 +0500

    ViewPager actvity added

[33mcommit ad9ef2b5440e8593d7d3670e0c10172f07126d01[m
Merge: 44123e1 e8a517e
Author: hussamh10 <hussamh10@gmail.com>
Date:   Mon Apr 17 20:57:36 2017 +0500

    SO MUCH CHANGES

[33mcommit 44123e1ed5f1e0e7b878cc5568c8f4786f6ae9b8[m
Author: hussamh10 <hussamh10@gmail.com>
Date:   Sun Apr 16 11:13:38 2017 +0500

    Added classes

[33mcommit e8a517efd16fbd2f596c5256b6292fd7d3be715b[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Sun Apr 16 02:04:07 2017 +0500

    App layout fixed

[33mcommit befbc0776e7e99f5afbb8630425f9f519047094c[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Sat Apr 15 23:48:55 2017 +0500

    Added basic swipe

[33mcommit 72e3b8fb64bdec50a6a031b879c291630ab8d3f9[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Sat Apr 15 22:57:40 2017 +0500

    Added images

[33mcommit f4a38559f51ed3636706f66d20e7c96a0eb0b1aa[m
Author: hussamh10 <hussamh10@gmail.com>
Date:   Sat Apr 15 22:42:16 2017 +0500

    Added classes

[33mcommit b0743c02388d69139e1afab465a32371d2fffbef[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Sat Apr 15 22:28:02 2017 +0500

    Major camera upgrade

[33mcommit 669c8b638062707bbad1c74f357ea366c2daf907[m
Author: hussamh10 <hussamh10@gmail.com>
Date:   Fri Apr 14 17:33:16 2017 +0500

    Added menus

[33mcommit c269e5236893bab4f6d478744f265cc37dda49d3[m
Author: hussamh10 <hussamh10@gmail.com>
Date:   Thu Apr 13 00:47:46 2017 +0500

    Added persistence to Message + Added Message Collection

[33mcommit b9709600c8c8a6ead36d9a97b5a0858da14b5c0a[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Wed Apr 12 23:17:01 2017 +0500

    Fixed color scheme

[33mcommit 7e304a399c38b5c232c3c0057a9f3fbc6fc60166[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Wed Apr 12 22:26:01 2017 +0500

    Updated thread view

[33mcommit a7579cbb080a0481c28ac2d43030fcfc346408b4[m
Author: hussamh10 <hussamh10@gmail.com>
Date:   Wed Apr 12 16:34:15 2017 +0500

    Added thread view structure

[33mcommit 3e7ceb284abb219e1f787ae174ebb9995163ad93[m
Author: hussamh10 <hussamh10@gmail.com>
Date:   Mon Mar 27 23:53:05 2017 +0500

    added camera

[33mcommit f9346716899104ca8a64f98bb8f71b8da5aa0ced[m
Author: hussamh10 <hussamh10@gmail.com>
Date:   Mon Mar 27 23:52:39 2017 +0500

    added camera

[33mcommit 2bc9b29133d18802f119aa16885d89f5fb4dd0c7[m
Author: hussamh10 <hussamh10@gmail.com>
Date:   Mon Mar 27 20:08:36 2017 +0500

    recovered

[33mcommit fbb7407bb585b33cb0b6ed37bae6ed8b284c3d1d[m
Author: hussamh10 <hussamh10@gmail.com>
Date:   Mon Mar 27 20:04:41 2017 +0500

    AI-2.2.3 <hussamh10@hussam-PC Create vim_settings.xml, IntelliLang.xml, ide.general.xml, git.xml, Default.xml, project.default.xml, diff.xml, laf.xml, other.xml, jdk.table.xml, debugger.renderers.xml, androidStudioFirstRun.xml, ui.lnf.xml, web-browsers.xml, keymap.xml, vcs.xml, androidEditors.xml, editor.codeinsight.xml, gradle.run.settings.xml, colors.scheme.xml, editor.xml, github_settings.xml, debugger.xml

[33mcommit cff8228aeb8f945e1e2f4a15ca16c42fb9976d45[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Mon Mar 27 15:22:38 2017 +0500

    Added gitignore

[33mcommit 4ffddeaa7630fef28fd13b0568528920c531ec70[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Mon Mar 27 14:13:02 2017 +0500

    Fixed gitignore

[33mcommit e3ca840d212801a996a958a2a8ab72b6c635b55d[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Mon Mar 27 12:34:02 2017 +0500

    Updated files

[33mcommit 5af57aa40bd1b16d8dc0e675b729f5140f0965e8[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Mon Mar 27 12:31:41 2017 +0500

    Updated gitignore

[33mcommit 57c0cff2433590ba5984e6fede39fe2d5047ea46[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Sun Mar 26 21:01:05 2017 +0500

    Added network tests

[33mcommit f2faa78a3f92d4efdfa750483278bcb852867595[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Sun Mar 26 20:59:46 2017 +0500

    Added testing classes

[33mcommit 9e80f53a3a5b6a577c2e75e1e06d08a2866860d9[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Sun Mar 26 01:40:10 2017 +0500

    Added missing classes (Untested)

[33mcommit ed5f923b72e31c0964a4a1a482772fa59a3d7f7b[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Sun Mar 26 01:39:43 2017 +0500

    Added complete networking (Untested)

[33mcommit 1f26fb28851926d0df176f8a14eefedaa55b4931[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Sat Mar 25 17:43:28 2017 +0500

    Cleaned AndroidManifest

[33mcommit c0ee05b1f0e0487257f7a85f5fd4fda0f78838ed[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Sat Mar 25 17:34:20 2017 +0500

    Added NSD Code (Untested)

[33mcommit 1ac43f6575e5ff7e88143adb690a2bec104dd62c[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Fri Mar 24 16:48:26 2017 +0500

    Added connection classes

[33mcommit 803a38df4fdcfa39f4ba48558009dc8cab97767c[m
Author: hussamh10 <hussamh10@gmail.com>
Date:   Thu Mar 23 03:56:53 2017 +0500

    added better git ignore

[33mcommit 0432de7a83a55cbb2feabd83314458721378d05d[m
Author: hussamh10 <hussamh10@gmail.com>
Date:   Thu Mar 23 03:55:56 2017 +0500

    added back

[33mcommit 31bf6310c3241294edbdada6d179e2cd19b68e59[m
Author: hussamh10 <hussamh10@gmail.com>
Date:   Thu Mar 23 03:36:27 2017 +0500

    removed files

[33mcommit 3b17fbfafedc622d2bd01b133c57d5cd11e31676[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Wed Mar 22 21:22:01 2017 +0500

    Added base packages

[33mcommit d7f77abd652ab5552924dff20e8e774b5ea552f8[m
Author: Muhammad Haroon <m_haroon96@hotmail.com>
Date:   Wed Mar 22 21:18:19 2017 +0500

    Added base project
