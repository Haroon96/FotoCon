package hussam_haroon.fotocon.shared;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.activities.RoomThreadActivity;
import hussam_haroon.fotocon.views.AvatarView;

/**
 * Created by M_har on 01-May-17.
 */

public class RoomAdapter extends ArrayAdapter<Room> {
	private Context context;

	public RoomAdapter(Context context) {
		super(context, 0, new ArrayList<Room>());
		this.context = context;
	}

	@Nullable
	@Override
	public Room getItem(int position) {
		return super.getItem(getCount() - position - 1);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		if (convertView == null) {
			convertView = ((Activity)context).getLayoutInflater().inflate(R.layout.room_history_row, parent, false);
			convertView.setTag(new RoomAdapter.ViewHolder((AvatarView)convertView.findViewById(R.id.avatar), (TextView)convertView.findViewById(R.id.room_name), (TextView)convertView.findViewById(R.id.host_name)));
		}

		final Room rsh = getItem(position);
		RoomAdapter.ViewHolder vh = (RoomAdapter.ViewHolder)convertView.getTag();

		vh.avatarView.setData(rsh.getHost().getName(), rsh.getHost().getPicture());

		vh.roomName.setText(rsh.getRoomName());
		vh.hostName.setText(rsh.getHostName());

		return convertView;
	}

	private static class ViewHolder {
		ViewHolder(AvatarView avatarView, TextView roomName, TextView hostName) {
			this.avatarView = avatarView;
			this.roomName = roomName;
			this.hostName = hostName;
		}
		public AvatarView avatarView;
		public TextView roomName;
		public TextView hostName;
	}
}
