package hussam_haroon.fotocon.shared;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hussam_haroon.fotocon.R;

/**
 * Created by M_har on 01-May-17.
 */

public class RoomServiceAdapter extends ArrayAdapter<RoomServiceHolder> {
	private Context context;

	public RoomServiceAdapter(Context context) {
		super(context, 0, new ArrayList<RoomServiceHolder>());
		this.context = context;
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		if (convertView == null) {
			convertView = ((Activity)context).getLayoutInflater().inflate(R.layout.room_list_row, parent, false);
			convertView.setTag(new ViewHolder((TextView)convertView.findViewById(R.id.room_name), (TextView)convertView.findViewById(R.id.host_name)));
		}

		RoomServiceHolder rsh = getItem(position);
		ViewHolder vh = (ViewHolder)convertView.getTag();

		vh.roomName.setText(rsh.getRoomName());
		vh.hostName.setText(rsh.getHostName());

		return convertView;
	}

	private class ViewHolder {
		ViewHolder(TextView roomName, TextView hostName) {
			this.roomName = roomName;
			this.hostName = hostName;
		}
		public TextView roomName;
		public TextView hostName;
	}
}