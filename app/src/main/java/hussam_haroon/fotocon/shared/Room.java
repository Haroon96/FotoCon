package hussam_haroon.fotocon.shared;

import android.content.Context;
import android.net.nsd.NsdServiceInfo;
import android.widget.ArrayAdapter;

import java.io.Serializable;
import java.util.ArrayList;

import hussam_haroon.fotocon.message.Message;
import hussam_haroon.fotocon.message.MessageCollection;

/**
 * Created by M_har on 24-Mar-17.
 */

public class Room implements Serializable {
	private String name;
	private String id;

	private Sender host;

	private ArrayList<Sender> senders;
	private MessageCollection messages;

	private boolean isAlive;

	public Room(String roomName, String id, Sender host) {
		this.name = roomName;
		this.host = host;
		this.id = id;
		senders = new ArrayList<>();
		messages = new MessageCollection();
		isAlive = true;
	}

	public Room(String roomName, Sender host) {
		this(roomName, "0", host);
	}

	public void persist(Context ctx) {
		this.id = String.valueOf(RoomCollection.getNextAvailableIndex());
		messages.save(id, ctx);
		host.setPicture(null);
		RoomCollection.getInstance().add(this);
		RoomCollection.getInstance().save();
	}

	public void remove() {
		for (Room r : RoomCollection.getInstance().getRooms()) {
			if (r.getId().equals(this.id)) {
				RoomCollection.getInstance().getRooms().remove(r);
				break;
			}
		}
	}

	public void addMessage(Message m){ messages.add(m);}

	public void addSender(Sender s){
		senders.add(s);
	}

	public ArrayList<Sender> getSenders(){
		return senders;
	}

	public MessageCollection getMessages(){return messages;}

	public String getRoomName() {
		return name;
	}

	public Sender getHost() {
		return host;
	}

	public String getHostName() {
		return host.getName();
	}

	public String getHostId(){
		return host.getId();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean equals(Room room) {
		return (this.id.equals(room.id));
	}

	public void removeSender(Sender s) {
		senders.remove(s);
	}

	public void loadMessages(Context ctx) {
		messages.load(id, ctx);
	}

	@Override
	public String toString() {
		return name + " | " + host.getName();
	}

	public Room strip() {
		Room room = new Room(name, id, host);
		room.messages = new MessageCollection();
		return room;
	}
}
