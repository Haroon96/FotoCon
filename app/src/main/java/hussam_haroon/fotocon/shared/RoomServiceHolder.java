package hussam_haroon.fotocon.shared;

import android.net.nsd.NsdServiceInfo;

/**
 * Created by M_har on 24-Apr-17.
 */

public class RoomServiceHolder {

	public RoomServiceHolder(String roomName, String hostName, NsdServiceInfo nsdServiceInfo) {
		this.roomName = roomName;
		this.hostName = hostName;
		this.nsdServiceInfo = nsdServiceInfo;
	}


	private String roomName;
	private String hostName;
	private NsdServiceInfo nsdServiceInfo;

	public String getRoomName() {
		return roomName;
	}

	public String getHostName() {
		return hostName;
	}

	public NsdServiceInfo getNsdServiceInfo() {
		return nsdServiceInfo;
	}

	@Override
	public boolean equals(Object obj) {
		RoomServiceHolder room = (RoomServiceHolder)obj;
		return (roomName.equals(room.roomName) && hostName.equals(room.hostName));
	}
}
