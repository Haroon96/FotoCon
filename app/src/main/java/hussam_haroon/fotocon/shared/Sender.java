package hussam_haroon.fotocon.shared;

/**
 * Created by hussamh10 on 15-Apr-17.
 */

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

/**
 *	Room has an array of senders
 *	We are Sender self in CameraActivity
 */

public class Sender implements Serializable {
	String id;
	String name;
	byte[] picture = null;

	public Sender(String name) {
		this("0", name);
	}

	public Sender(String id, String name){
		this.id = id;
		this.name = name;
	}

	public void setPicture(Bitmap picture){
		if (picture == null) {
			return;
		}
		ByteArrayOutputStream blob = new ByteArrayOutputStream();
		picture.compress(Bitmap.CompressFormat.PNG, 0 /* Ignored for PNGs */, blob);
		this.picture = blob.toByteArray();
	}

	public Bitmap getPicture() {
		if (picture == null) {
			return null;
		}
		return BitmapFactory.decodeByteArray(this.picture, 0, this.picture.length);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		return id.equals(((Sender)obj).id);
	}
}
