package hussam_haroon.fotocon.shared;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.views.AvatarView;

/**
 * Created by M_har on 01-May-17.
 */

public class SenderAdapter extends ArrayAdapter<Sender> {
	private Context context;

	public SenderAdapter(Context context) {
		super(context, 0, new ArrayList<Sender>());
		this.context = context;
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		if (convertView == null) {
			convertView = ((Activity)context).getLayoutInflater().inflate(R.layout.member_list_row, parent, false);
			convertView.setTag(new SenderAdapter.ViewHolder((AvatarView)convertView.findViewById(R.id.avatar), (TextView)convertView.findViewById(R.id.member_name)));
		}

		Sender s = getItem(position);
		SenderAdapter.ViewHolder vh = (SenderAdapter.ViewHolder)convertView.getTag();

		vh.avatarView.setData(s.getName(), s.getPicture());
		vh.hostName.setText(s.getName());

		return convertView;
	}

	private static class ViewHolder {
		ViewHolder(AvatarView avatarView, TextView hostName) {
			this.avatarView = avatarView;
			this.hostName = hostName;
		}
		public AvatarView avatarView;
		public TextView hostName;
	}
}
