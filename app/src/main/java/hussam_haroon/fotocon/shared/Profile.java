package hussam_haroon.fotocon.shared;

/**
 * Created by hussamh10 on 15-Apr-17.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.Settings.Secure;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.Serializable;

import hussam_haroon.fotocon.R;


/*
	This is a singleton class that stores the sender date of the user and the current room the
	user is connected to
	This class is initialized in the very first activity.
 */

public class Profile implements Serializable {

	public static final String FILENAME = "fotocon.profile";

	private Sender self = null;
	private BitmapDrawable wallpaperDrawable = null;
	private byte[] wallpaper = null;

	private static Profile singleton = null;

	private Profile(String name){
		if (self == null) {
			self = new Sender("0", name);
		}
	}

	public static Profile init(String name){
		if (singleton == null){
			singleton = new Profile(name);
		}
		return singleton;
	}

	public Drawable getDrawableWallpaper() {
		return wallpaperDrawable;
	}

	public void setWallpaper(Bitmap bitmap) {
		this.wallpaperDrawable = new BitmapDrawable(bitmap);
	}

	public static Profile getInstance(){
		return singleton;
	}

	public Sender getSelf(){
		return self;
	}

	public static void save(final Context context, final Runnable callback) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (singleton.wallpaperDrawable != null) {
						Bitmap bitmap = singleton.wallpaperDrawable.getBitmap();

						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						bitmap.compress(Bitmap.CompressFormat.PNG, 0, baos);
						singleton.wallpaper = baos.toByteArray();
					}
					BitmapDrawable tmp = singleton.wallpaperDrawable;
					singleton.wallpaperDrawable = null;

					ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(new File(context.getFilesDir(), FILENAME)));
					objectOutputStream.writeObject(singleton);

					singleton.wallpaperDrawable = tmp;
					singleton.wallpaper = null;

					if (callback != null) {
						callback.run();
					}
				} catch (Exception e){}

			}
		}).start();
	}

	public static void save(final Context context) {
		save(context, null);
	}

	public static boolean exists(Context context) {
		return (new File(context.getFilesDir(), FILENAME)).exists();
	}

	public static void load(final Context context, final Runnable callback) {
		if (singleton != null) {
			// already init
			if (callback != null) {
				callback.run();
			}
			return;
		}
		// load profile
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(new File(context.getFilesDir(), FILENAME)));
					singleton = (Profile)objectInputStream.readObject();
					if (singleton.wallpaper != null) {
						singleton.wallpaperDrawable = new BitmapDrawable(BitmapFactory.decodeByteArray(singleton.wallpaper, 0, singleton.wallpaper.length));
					}
					if (callback != null) {
						callback.run();
					}
					objectInputStream.close();
				} catch (Exception e){}
			}
		}).start();
	}
}
