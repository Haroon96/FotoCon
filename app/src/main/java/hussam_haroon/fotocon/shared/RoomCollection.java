package hussam_haroon.fotocon.shared;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by hussamh10 on 15-Apr-17.
 */

public class RoomCollection {


	// ----------------------------
	// Singleton implementation
	// ----------------------------

	private static RoomCollection roomCollection = null;

	public static void init(Context context) {
		if (roomCollection == null) {
			roomCollection = new RoomCollection(context);
		}
	}
	public static RoomCollection getInstance() {
		return roomCollection;
	}

	private RoomCollection(final Context context) {
		this.rooms = new ArrayList<>();
		new Thread(new Runnable() {
			@Override
			public void run() {
				sqlDb = new DbHelper(context).getReadableDatabase();
			}
		}).start();
	}

	// ----------------------------


	private ArrayList<Room> rooms = null;
	private SQLiteDatabase sqlDb = null;
	private static int nextAvailableIndex = 0;
	private boolean loaded = false;

	public boolean load() {
		return load(null);
	}

	public boolean load(final Runnable callback) {
		if (loaded) {
			if (callback != null) {
				callback.run();
			}
			return false;
		}
		new Thread(new Runnable() {
			@Override
			public void run() {
				rooms.clear();

				while (sqlDb == null) {}

				Cursor cursor = sqlDb.query(DbHelper.Rooms.tableName,
						new String[]{DbHelper.Rooms.idCol, DbHelper.Rooms.roomNameCol, DbHelper.Rooms.hostNameCol},
						null, null, null, null, DbHelper.Rooms.idCol + " ASC"
						);

				int idColIndex = 0;
				int roomNameColIndex = 1;
				int hostNameColIndex = 2;

				while (cursor.moveToNext()) {
					rooms.add(new Room(cursor.getString(roomNameColIndex), cursor.getString(idColIndex), new Sender(cursor.getString(hostNameColIndex))));
					if (cursor.isLast()) {
						nextAvailableIndex = Integer.parseInt(cursor.getString(idColIndex)) + 1;
					}
				}
				loaded = true;
				if(callback != null) {
					callback.run();
				}
			}
		}).start();
		return true;
	}

	public void save() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				// truncate table
				sqlDb.delete(DbHelper.Rooms.tableName, null, null);

				ContentValues contentValues = new ContentValues();

				for (Room room : rooms) {
					contentValues.put(DbHelper.Rooms.idCol, room.getId());
					contentValues.put(DbHelper.Rooms.roomNameCol, room.getRoomName());
					contentValues.put(DbHelper.Rooms.hostNameCol, room.getHostName());
					sqlDb.insert(DbHelper.Rooms.tableName, null, contentValues);
				}
			}
		}).start();
	}

	public void add(Room room) {
		rooms.add(room);
	}

	public static int getNextAvailableIndex() {
		return nextAvailableIndex;
	}

	public ArrayList<Room> getRooms(){
		return rooms;
	}

	public int getSize(){
		return rooms.size();
	}

	public Room getRoom(String roomId) {
		for (Room r : rooms) {
			if (r.getId().equals(roomId)) {
				return r;
			}
		}
		return null;
	}

	public static class DbHelper extends SQLiteOpenHelper {

		private static final String dbName = "FotoCon";

		public DbHelper(Context context) {
			super(context, dbName, null, 33);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			String constructorQuery = "CREATE TABLE " + Rooms.tableName + "(" +
					Rooms.idCol + " TEXT KEY," +
					Rooms.roomNameCol + " TEXT," +
					Rooms.hostNameCol + " TEXT)";
			db.execSQL(constructorQuery);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		}

		public static class Rooms {
			public final static String tableName = "Rooms";
			public final static String idCol = "RoomId";
			public final static String roomNameCol = "RoomName";
			public final static String hostNameCol = "HostName";
		}

	}
}
