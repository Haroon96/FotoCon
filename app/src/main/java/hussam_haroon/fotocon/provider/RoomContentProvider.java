package hussam_haroon.fotocon.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import hussam_haroon.fotocon.shared.RoomCollection;

/**
 * Created by hussamh10 on 04-May-17.
 */

public class RoomContentProvider extends ContentProvider{
	private static UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
	private SQLiteDatabase db;
	private RoomCollection.DbHelper helper;

	static {
		matcher.addURI("hussam_haroon.fotocon.roomprovider","room",1);
	}

	@Override
	public boolean onCreate() {
		helper = new RoomCollection.DbHelper(getContext());
		db = helper.getReadableDatabase();
		return true;
	}

	@Nullable
	@Override
	public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
		if(matcher.match(uri) == 1){
			return db.query("Rooms",projection,selection,selectionArgs,null,null,sortOrder);
		}
		String[] cols = {"_ID"};
		return new MatrixCursor(cols);
	}

	@Nullable
	@Override
	public String getType(@NonNull Uri uri) {
		return null;
	}

	@Nullable
	@Override
	public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
		return null;
	}

	@Override
	public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
		return 0;
	}

	@Override
	public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
		return 0;
	}
}
