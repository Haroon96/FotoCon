package hussam_haroon.fotocon.exceptions;

/**
 * Created by M_har on 26-Mar-17.
 */

public class RoomSearchException extends Exception {
	public RoomSearchException(String message) {
		super(message);
	}
}
