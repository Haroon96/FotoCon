package hussam_haroon.fotocon.exceptions;

/**
 * Created by M_har on 26-Mar-17.
 */

public class RoomHostException extends Exception {
	public RoomHostException(String message) {
		super(message);
	}
}
