package hussam_haroon.fotocon.message;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.activities.utilities.Helpers;
import hussam_haroon.fotocon.activities.views.MessageView;
import hussam_haroon.fotocon.shared.Sender;

/**
 * Created by M_har on 24-Apr-17.
 */

public class System extends Message {
	public final static String SYSTEM_SENDER = "SYSTEM";

	public Sender getSenderObj() {
		return sender;
	}

	public enum State {JOIN, LEFT};

	private State state;
	private Sender sender;
	private String message;

	public System(Sender sender, State state) {
		super(SYSTEM_SENDER, "");
		this.sender = sender;
		this.state = state;
		setIs_image(false);
		byte[] arr;
		if (state == State.JOIN) {
			this.message = (sender.getName() + " has joined the room");
		} else {
			this.message = (sender.getName() + " has left the room");
		}
	}

	public State getState() {
		return state;
	}

	@Override
	public View getView(Context ctx) {
		LinearLayout view = (LinearLayout)((Activity)ctx).getLayoutInflater().inflate(R.layout.system_message, null);
		((TextView)view.findViewById(R.id.message)).setText(message);
		return view;
	}
}
