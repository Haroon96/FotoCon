package hussam_haroon.fotocon.message;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.activities.views.MessageView;

/**
 * Created by hussamh10 on 12-Apr-17.
 */

public class Image extends Message {

	public Image(String sender, String date, Bitmap image){
		super(sender, date);
		setIs_image(true);
		byte[] arr = toByteArray(image);
		setData(arr);
	}

	public Image(String sender, String date, byte[] data){
		super(sender, date);
		setIs_image(true);
		setData(data);
	}

	public static byte[] toByteArray(Bitmap image){
		ByteArrayOutputStream blob = new ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.PNG, 0 /* Ignored for PNGs */, blob);
		byte[] bitmapdata = blob.toByteArray();
		return bitmapdata;
	}

	public static Bitmap toBitmap(byte[] data){
		Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
		return bitmap;
	}

	public static Bitmap getThumbnail(byte[] data) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inSampleSize = 5;
		options.inJustDecodeBounds = false;
		options.inTempStorage = new byte[16 * 1024];

		return BitmapFactory.decodeByteArray(data, 0, data.length, options);
	}

	public Bitmap getBitmap(){
		return toBitmap(getData());
	}

	@Override
	public View getView(Context ctx) {
		MessageView view = (MessageView)((Activity)ctx).getLayoutInflater().inflate(R.layout.message_layout, null);
		if (isSender()) {
			// is sender
			view.sentImage(getThumbnail(getData()), getDate());
		} else {
			// is not sender
			view.receivedImage(getThumbnail(getData()), getSenderName(), getDate());
		}
		return view;
	}
}
