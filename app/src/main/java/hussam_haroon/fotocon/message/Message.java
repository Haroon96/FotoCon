package hussam_haroon.fotocon.message;

import android.content.Context;
import android.view.View;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.util.UUID;

/**
 * Created by hussamh10 on 12-Apr-17.
 */

public abstract class Message implements Serializable {
	private String date;
	private String senderName;
	private String id;
	private boolean isSender = true;
	private boolean is_image;
	private byte[] data;


	public Message(String sender, String date){
		this.senderName = sender;
		this.date = date;
		this.data = null;
		this.id = UUID.randomUUID().toString();
	}

	public boolean isSender() {
		return isSender;
	}

	public String getId() {
		return id;
	}

	public void setIsSender(boolean isSender) {
		this.isSender = isSender;
	}

	public abstract View getView(Context ctx);

	// Getter Setters

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String sender) {
		this.senderName = sender;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public void setIs_image(boolean b){ is_image = b;}

	public boolean isImage(){return is_image;}
}
