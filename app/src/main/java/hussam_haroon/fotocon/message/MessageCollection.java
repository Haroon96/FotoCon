package hussam_haroon.fotocon.message;

import android.content.Context;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by hussamh10 on 13-Apr-17.
 */

public class MessageCollection implements Serializable {
	public ArrayList<Message> messages;

	public MessageCollection(){
		messages = new ArrayList<>();
	}

	public int size(){
		return messages.size();
	}

	public void add(Message m){
		messages.add(m);
	}

	public void load(String roomId, Context ctx){
		File file = new File(ctx.getFilesDir(), roomId);
		Message[] temp;
		try{
			ObjectInputStream os = new ObjectInputStream(new FileInputStream(file));
			messages = (ArrayList<Message>)os.readObject();
			os.close();
		}
		catch (Exception e){
			Log.e("FILING", "save: Error while opening or reading from file (Custom error)");
		}
	}

	public void save(String roomId, Context ctx){
		File file = new File(ctx.getFilesDir(), roomId);
		try{
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(file));
			os.writeObject(messages);
			os.close();
		}
		catch (Exception e){
			Log.e("FILING", "save: Error while opening or writing to file (Custom error)");
		}
	}
}
