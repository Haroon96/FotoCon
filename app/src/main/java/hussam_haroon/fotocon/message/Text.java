package hussam_haroon.fotocon.message;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.activities.views.MessageView;

/**
 * Created by hussamh10 on 12-Apr-17.
 */

public class Text extends Message {
	public Text(String sender, String date, String text){
		super(sender, date);
		setIs_image(false);
		byte[] arr = toByteArray(text);
		setData(arr);
}

	public static String toString(byte[] data){
		String str = new String(data);
		return str;
	}

	public static byte[] toByteArray(String str){
		return str.getBytes();
	}

	@Override
	public View getView(Context ctx) {
		MessageView view = (MessageView)((Activity)ctx).getLayoutInflater().inflate(R.layout.message_layout, null);
		if (isSender()) {
			// is sender
			view.sentMessage(new String(getData()), getDate());
		} else {
			// is not sender
			view.receivedMessage(new String(getData()), getSenderName(), getDate());
		}
		return view;
	}
}
