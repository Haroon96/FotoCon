package hussam_haroon.fotocon.connection;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.UUID;

/**
 * Created by M_har on 30-Apr-17.
 */

public class SocketHolder {
	public SocketHolder(Socket socket, ObjectInputStream objectInputStream, ObjectOutputStream objectOutputStream) {
		this.socket = socket;
		this.objectInputStream = objectInputStream;
		this.objectOutputStream = objectOutputStream;
		this.id = UUID.randomUUID().toString();
	}
	public Socket socket;
	public ObjectInputStream objectInputStream;
	public ObjectOutputStream objectOutputStream;
	public String id;
}
