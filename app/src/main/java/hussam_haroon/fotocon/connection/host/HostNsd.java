package hussam_haroon.fotocon.connection.host;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;

import java.net.InetAddress;

/**
 * Created by M_har on 17-Apr-17.
 */

class HostNsd {

	private final static String SERVICE_TYPE = "_http._tcp.";
	private final static String SERVICE_ID = "FOTOCON";
	private final static String delimiter = "$";

	HostNsd(Context context, int PORT) {
		this.nsdManager = (NsdManager)context.getSystemService(Context.NSD_SERVICE);
		this.PORT = PORT;
		initRegistrationListener();
	}

	public void hostRoom(String roomName, String hostName) {

		NsdServiceInfo serviceInfo = new NsdServiceInfo();
		String serviceName = SERVICE_ID + roomName + '$' + hostName;
		serviceInfo.setServiceName(serviceName);
		serviceInfo.setServiceType(SERVICE_TYPE);
		serviceInfo.setPort(PORT);

		nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD, registrationListener);
	}

	private void initRegistrationListener() {
		registrationListener = new NsdManager.RegistrationListener() {
			@Override
			public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {}

			@Override
			public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {}

			@Override
			public void onServiceRegistered(NsdServiceInfo serviceInfo) {
				// service is running now
			}
			@Override
			public void onServiceUnregistered(NsdServiceInfo serviceInfo) {
				// service no longer visible on network
			}
		};
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		stopHosting();
	}

	public void stopHosting() {
		nsdManager.unregisterService(registrationListener);
	}

	private NsdManager nsdManager;
	private NsdManager.RegistrationListener registrationListener;

	private int PORT;

}
