package hussam_haroon.fotocon.connection.host;

import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import hussam_haroon.fotocon.activities.PagerActivity;
import hussam_haroon.fotocon.connection.ConnectionStatusListener;
import hussam_haroon.fotocon.connection.MemberStateChangeListener;
import hussam_haroon.fotocon.connection.MessageListener;
import hussam_haroon.fotocon.connection.ServiceBinder;
import hussam_haroon.fotocon.connection.SocketHolder;
import hussam_haroon.fotocon.message.Message;
import hussam_haroon.fotocon.message.System;
import hussam_haroon.fotocon.shared.Room;
import hussam_haroon.fotocon.shared.Sender;


/**
 * Created by M_har on 17-Apr-17.
 */

class HostSocketComms extends Thread {

	private int nextId = 1;

	private HostService service;

	private ArrayList<SocketHolder> clientSockets;

	HostSocketComms(int PORT, HostService service) {
		this.PORT = PORT;
		this.service = service;
		clientSockets = new ArrayList<>();
	}

	protected void disposeSocket(Socket socket) {
		try {
			socket.close();
		} catch (Exception e) {

		} finally {
			synchronized (clientSockets) {
				clientSockets.remove(socket);
			}
		}
	}

	public void disconnect() {
		closeConnection();
	}

	protected void closeConnection() {
		try {
			serverSkt.close();
			for (SocketHolder s : clientSockets) {
				disposeSocket(s.socket);
			}
		} catch (Exception e) {

		}
	}

	@Override
	public void run() {
		listenForClients();
	}

	public void listenForClients() {
		try {
			serverSkt = new ServerSocket(PORT);
			while (true) {
				try {
					// process the new socket in a separate thread
					processSocket(serverSkt.accept());
				} catch (Exception e) {
					if (serverSkt.isClosed()) {
						return;
					}
				}
			}
		} catch (Exception e) {}
	}

	private void processSocket(final Socket socket) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				int tmpId = nextId++;
				try {
					ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
					ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());

					final SocketHolder socketHolder = new SocketHolder(socket, objectInputStream, objectOutputStream);

					// read sender data from stream
					Sender sender = (Sender)objectInputStream.readObject();

					// assign id to sender
					objectOutputStream.writeObject(new Integer(tmpId));
					sender.setId(Integer.toString(tmpId));

					// save sender
					memberStateChangeListener.memberJoined(sender);

					// synchronize the sender's room
					objectOutputStream.writeObject(service.getRoom().strip());

					// begin listening to sender
					new HostDataReceiver(socketHolder, sender).start();

					// update the new client
					new Thread(new Runnable() {
						@Override
						public void run() {
							for (Message m : service.getRoom().getMessages().messages) {
								try {
									socketHolder.objectOutputStream.writeObject(m);
								} catch (Exception e) {

								}
							}
						}
					}).start();

					synchronized(clientSockets) {
						clientSockets.add(socketHolder);
					}

				} catch (Exception e){}
			}
		}).start();
	}

	private ServerSocket serverSkt = null;
	private int PORT;

	public void forwardMessage(final Message m, final SocketHolder socketHolder) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				boolean send;
				for (SocketHolder sh : clientSockets) {
					send = false;
					if (socketHolder == null) {
						send = true;
					} else if (!sh.id.equals(socketHolder.id)) {
						send = true;
					}
					if (send) {
						new HostDataSender(sh, m).start();
					}
				}
			}
		}).start();
	}

	public void forwardMessage(Message m) {
		try {
			forwardMessage(m, null);
		} catch (Exception e) {

		}
	}

	private class HostDataReceiver extends Thread {

		private SocketHolder socketHolder;
		private Sender sender;

		public HostDataReceiver(SocketHolder socketHolder, Sender sender) {
			this.socketHolder = socketHolder;
			this.sender = sender;
		}
		@Override
		public void run() {
			try {
				while(true) {
					Message m = (Message)socketHolder.objectInputStream.readObject();
					m.setIsSender(false);
					messageListener.messageReceived(m);
					forwardMessage(m, socketHolder);
				}
			} catch(Exception e) {
				if (!serverSkt.isClosed()) {
					disposeSocket(socketHolder.socket);
					memberStateChangeListener.memberLeft(sender);
				}
			}
		}
	}

	private class HostDataSender extends Thread {
		private SocketHolder socketHolder;
		private Message message;

		public HostDataSender(SocketHolder socketHolder, Message message) {
			try {
				this.socketHolder = socketHolder;
				this.message = message;
			} catch (Exception e) {}
		}
		@Override
		public void run() {
			try {
				socketHolder.objectOutputStream.writeObject(message);
			} catch(Exception e) {}
		}
	}

	private MessageListener messageListener = null;
	private MemberStateChangeListener memberStateChangeListener = null;

	public void setMessageListener(MessageListener messageListener) {
		this.messageListener = messageListener;
	}
	public void setMemberStateChangeListener(MemberStateChangeListener memberStateChangeListener) {
		this.memberStateChangeListener = memberStateChangeListener;
	}
}
