package hussam_haroon.fotocon.connection.host;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.NotificationCompat;
import android.widget.Toast;

import java.util.Random;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.activities.PagerActivity;
import hussam_haroon.fotocon.connection.MemberStateChangeListener;
import hussam_haroon.fotocon.connection.MessageListener;
import hussam_haroon.fotocon.connection.ServiceBinder;
import hussam_haroon.fotocon.message.Message;
import hussam_haroon.fotocon.message.System;
import hussam_haroon.fotocon.shared.Room;
import hussam_haroon.fotocon.shared.Sender;


/**
 * Created by M_har on 17-Apr-17.
 */

public class HostService extends Service {

	private Sender me;
	private static Handler handler;

	private static ServiceBinder binder;
	private static final int NOTIFICATION_ID = 2305;

	private static Room room;

	private static boolean isRunning = false;

	private BroadcastReceiver broadcastReceiver;

	private static String STOP_SERVICE;


	private void runAsForeground() {
		STOP_SERVICE = getString(R.string.stop_service);

		Intent notificationIntent = new Intent(this, PagerActivity.class);
		Intent disconnectNotificationIntent = new Intent(STOP_SERVICE);

		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		PendingIntent disconnectIntent = PendingIntent.getBroadcast(this, 0, disconnectNotificationIntent, 0);

		Notification notification = new NotificationCompat.Builder(this)
				.setSmallIcon(R.mipmap.ic_launcher)
				.setContentTitle("FotoCon")
				.setContentText("You are hosting a room")
				.addAction(R.drawable.ic_close_black_24dp, "Stop Hosting", disconnectIntent)
				.setContentIntent(pendingIntent).build();

		startForeground(NOTIFICATION_ID, notification);
	}

	@Override
	public int onStartCommand(final Intent intent, int flags, int startId) {

		runAsForeground();

		handler = new Handler();
		new Thread(new Runnable() {
			@Override
			public void run() {

				broadcastReceiver = new BroadcastReceiver() {
					@Override
					public void onReceive(Context context, Intent intent) {
						if (intent.getAction().equals(STOP_SERVICE)) {
							closeRoom();
						}
					}
				};

				registerReceiver(broadcastReceiver, new IntentFilter(STOP_SERVICE));

				binder = new HostBinder();
				isRunning = true;

				String roomName = (String)intent.getSerializableExtra("roomName");
				String hostName = (String)intent.getSerializableExtra("hostName");
				me = (Sender)intent.getSerializableExtra("Me");

				me.setId(Integer.toString(0));

				room = new Room(roomName, me);

				room.addSender(me);

				persistMessage(new System(me, System.State.JOIN));

				startHosting(roomName, hostName);

				sendBroadcast(new Intent(getString(R.string.service_state_change)));

			}
		}).start();

		return START_NOT_STICKY;
	}

	private void startHosting(final String roomName, final String hostName) {
		int PORT = Math.abs((new Random().nextInt() % 40000) + 3000);
		hostNsd = new HostNsd(HostService.this, PORT);
		hostNsd.hostRoom(roomName, hostName);

		hostSocketComms = new HostSocketComms(PORT, this);
		hostSocketComms.setMemberStateChangeListener(new MemberStateChangeListener() {
			@Override
			public void memberJoined(final Sender s) {
				room.addSender(s);
				handler.post(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(HostService.this, s.getName() + " has joined the room", Toast.LENGTH_SHORT).show();
					}
				});
				binder.sendMessage(new System(s, System.State.JOIN));
			}

			@Override
			public void memberLeft(final Sender s) {
				room.removeSender(s);
				handler.post(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(HostService.this, s.getName() + " has left the room", Toast.LENGTH_SHORT).show();
					}
				});
				binder.sendMessage(new System(s, System.State.LEFT));
			}
		});
		hostSocketComms.setMessageListener(new MessageListener() {
			@Override
			public void messageReceived(Message m) {
				// already forwarded to clients
				persistMessage(m);
				broadcastMessage(m);
			}
		});
		hostSocketComms.start();
	}

	private void persistMessage(Message m) {
		room.addMessage(m);
	}

	private void broadcastMessage(Message m) {
		binder.notifyListeners(m);
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
//		closeRoom();
	}

	private void closeRoom() {
		if (hostNsd != null) {
			hostNsd.stopHosting();
			hostNsd = null;
		}
		if (hostSocketComms != null) {
			hostSocketComms.disconnect();
			hostSocketComms = null;
		}
		room.persist(this);
		unregisterReceiver(broadcastReceiver);
		sendBroadcast(new Intent(getString(R.string.service_state_change)));
		isRunning = false;
		stopSelf();
		stopForeground(true);
	}

	private HostNsd hostNsd = null;
	private HostSocketComms hostSocketComms = null;

	public static boolean isRunning() {
		return isRunning;
	}

	private class HostBinder extends ServiceBinder {
		public void sendMessage(Message m) {
			hostSocketComms.forwardMessage(m);
			persistMessage(m);
			broadcastMessage(m);
		}
		public void disconnect() {
			closeRoom();
		}
		public Room getRoom() {
			return room;
		}
		public String getSenderId() {
			return "0";
		}
		public void registerMessageCallback(MessageListener l) {
			this.listener = l;
		}

		public void notifyListeners(Message m) {
			if (listener != null) {
				listener.messageReceived(m);
			}
		}

		public void unregisterMessageCallback() {
			listener = null;
		}

		private MessageListener listener = null;
	}

	public Room getRoom() {
		return room;
	}

}
