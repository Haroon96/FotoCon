package hussam_haroon.fotocon.connection;

import hussam_haroon.fotocon.message.Message;
import hussam_haroon.fotocon.shared.Room;

/**
 * Created by M_har on 28-Apr-17.
 */

public abstract class ServiceBinder extends android.os.Binder {
	public abstract void sendMessage(Message m);
	public abstract void disconnect();
	public abstract Room getRoom();
	public abstract String getSenderId();
	public abstract void registerMessageCallback(MessageListener listener);
	public abstract void notifyListeners(Message m);
	public abstract void unregisterMessageCallback();
}
