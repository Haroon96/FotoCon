package hussam_haroon.fotocon.connection;

import hussam_haroon.fotocon.message.Message;

/**
 * Created by M_har on 28-Apr-17.
 */

public interface MessageListener {
	void messageReceived(Message m);
}
