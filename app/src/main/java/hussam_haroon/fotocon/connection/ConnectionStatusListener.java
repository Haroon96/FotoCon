package hussam_haroon.fotocon.connection;

/**
 * Created by M_har on 30-Apr-17.
 */

public interface ConnectionStatusListener {
	void connectionDropped();
}
