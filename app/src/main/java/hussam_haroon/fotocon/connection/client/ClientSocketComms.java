package hussam_haroon.fotocon.connection.client;

import java.io.DataInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

import hussam_haroon.fotocon.connection.ConnectionStatusListener;
import hussam_haroon.fotocon.connection.MemberStateChangeListener;
import hussam_haroon.fotocon.connection.MessageListener;
import hussam_haroon.fotocon.connection.SocketHolder;
import hussam_haroon.fotocon.message.Message;
import hussam_haroon.fotocon.message.System;

/**
 * Created by M_har on 24-Apr-17.
 */

class ClientSocketComms {

	private SocketHolder socketHolder = null;

	ClientSocketComms(SocketHolder socketHolder) {
		this.socketHolder = socketHolder;
	}

	public void startReceiving() {
		new DataReceiver().start();
	}

	public void disconnect() {
		try {
			if (socketHolder != null) {
				socketHolder.objectInputStream.close();
				socketHolder.objectOutputStream.close();
				socketHolder.socket.close();
				connectionStatusListener.connectionDropped();
				socketHolder = null;
			}
		} catch (Exception e) {}
	}

	public void sendMessage(final Message m) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					socketHolder.objectOutputStream.writeObject(m);
				} catch (Exception e){}
			}
		}).start();
	}

	private class DataReceiver extends Thread {
		@Override
		public void run() {
			try {
				while(true) {
					Message m = (Message)socketHolder.objectInputStream.readObject();
					m.setIsSender(false);
					messageListener.messageReceived(m);
					if (m.getSenderName().equals(System.SYSTEM_SENDER)) {
						System sys_msg = (System)m;
						if (sys_msg.getState() == System.State.JOIN) {
							memberStateChangeListener.memberJoined(sys_msg.getSenderObj());
						} else {
							memberStateChangeListener.memberLeft(sys_msg.getSenderObj());
						}
					}
				}
			} catch(Exception e) {
				disconnect();
			}
		}
	}

	private MessageListener messageListener = null;
	private MemberStateChangeListener memberStateChangeListener = null;
	private ConnectionStatusListener connectionStatusListener = null;
	public void setMemberStateChangeListener(MemberStateChangeListener memberStateChangeListener) {
		this.memberStateChangeListener = memberStateChangeListener;
	}
	public void setMessageListener(MessageListener messageListener) {
		this.messageListener = messageListener;
	}
	public void setConnectionStatusListener(ConnectionStatusListener connectionStatusListener) {
		this.connectionStatusListener = connectionStatusListener;
	}

}
