package hussam_haroon.fotocon.connection.client;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.widget.Toast;

import java.net.InetAddress;

import hussam_haroon.fotocon.activities.MainActivity;
import hussam_haroon.fotocon.shared.RoomServiceHolder;
/**
 * Created by M_har on 24-Apr-17.
 */

public class ClientNsd {

	private final String SERVICE_TYPE = "_http._tcp.";
	private final String SERVICE_ID = "FOTOCON";
	private final String delimiter = "$";

	private NsdManager nsdManager;
	private NsdManager.DiscoveryListener discoveryListener = null;
	private NsdManager.ResolveListener resolveListener = null;

	public ClientNsd(Context context) {
		this.nsdManager = (NsdManager)context.getSystemService(Context.NSD_SERVICE);
	}

	public void startSearch() {
		initDiscoveryListener();
		initResolveListener();
		nsdManager.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, discoveryListener);
	}

	private void initDiscoveryListener() {

		discoveryListener = new NsdManager.DiscoveryListener() {
			@Override
			public void onStartDiscoveryFailed(String serviceType, int errorCode) {}

			@Override
			public void onStopDiscoveryFailed(String serviceType, int errorCode) {}

			@Override
			public void onDiscoveryStarted(String serviceType) {}

			@Override
			public void onDiscoveryStopped(String serviceType) {}

			@Override
			public void onServiceFound(final NsdServiceInfo serviceInfo) {
				if (!serviceInfo.getServiceType().equals(SERVICE_TYPE))
					return;
				if (!serviceInfo.getServiceName().contains(SERVICE_ID))
					return;
				if (roomFoundListener != null) {
					// get ServiceName
					// trim SERVICE_ID from ServiceName and store
					String serviceName = serviceInfo.getServiceName().replace(SERVICE_ID, "");
					// extract room and host names from serviceName
					int delimiterIndex = serviceName.indexOf(delimiter);
					final String roomName = serviceName.substring(0, delimiterIndex);
					final String hostName = serviceName.substring(delimiterIndex + delimiter.length(), serviceName.length());

					roomFoundListener.roomFound(new RoomServiceHolder(roomName, hostName, serviceInfo));
				}
			}
			@Override
			public void onServiceLost(final NsdServiceInfo serviceInfo) {
				if (!serviceInfo.getServiceName().contains(SERVICE_ID))
					return;
				if (roomFoundListener != null) {
					// get ServiceName
					// trim SERVICE_ID from ServiceName and store
					String serviceName = serviceInfo.getServiceName().replace(SERVICE_ID, "");

					// extract room and host names from serviceName
					int delimiterIndex = serviceName.indexOf(delimiter);
					final String roomName = serviceName.substring(0, delimiterIndex);
					final String hostName = serviceName.substring(delimiterIndex + delimiter.length(), serviceName.length());

					roomFoundListener.roomLost(new RoomServiceHolder(roomName, hostName, serviceInfo));

				}
			}
		};
	}

	private void initResolveListener() {
		resolveListener = new NsdManager.ResolveListener() {
			@Override
			public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
			}

			@Override
			public void onServiceResolved(NsdServiceInfo serviceInfo) {
				serviceResolvedListener.params(serviceInfo.getHost(), serviceInfo.getPort());
			}
		};
	}

	public void connect(RoomServiceHolder rsh) {
		nsdManager.resolveService(rsh.getNsdServiceInfo(), resolveListener);
	}

	public interface RoomFoundListener {
		void roomFound(RoomServiceHolder room);
		void roomLost(RoomServiceHolder room);
	}
	public interface ServiceResolvedListener {
		void params(InetAddress host, int port);
	}

	private RoomFoundListener roomFoundListener = null;
	private ServiceResolvedListener serviceResolvedListener = null;

	public void setRoomFoundListener(RoomFoundListener roomFoundListener) {
		this.roomFoundListener = roomFoundListener;
	}
	public void setServiceResolvedListener(ServiceResolvedListener serviceResolvedListener) {
		this.serviceResolvedListener = serviceResolvedListener;
	}


}
