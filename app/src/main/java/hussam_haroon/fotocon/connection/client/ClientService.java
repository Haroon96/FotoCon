package hussam_haroon.fotocon.connection.client;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.widget.Toast;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Random;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.activities.PagerActivity;
import hussam_haroon.fotocon.connection.ConnectionStatusListener;
import hussam_haroon.fotocon.connection.MemberStateChangeListener;
import hussam_haroon.fotocon.connection.MessageListener;
import hussam_haroon.fotocon.connection.ServiceBinder;
import hussam_haroon.fotocon.connection.SocketHolder;
import hussam_haroon.fotocon.message.Message;
import hussam_haroon.fotocon.shared.Room;
import hussam_haroon.fotocon.shared.Sender;

/**
 * Created by M_har on 24-Apr-17.
 */

public class ClientService extends Service {

	private Sender me;
	private ServiceBinder binder;
	private Room room;
	private String myId;

	private Handler handler;
	private BroadcastReceiver broadcastReceiver;

	private static String STOP_SERVICE;

	private static final int NOTIFICATION_ID = new Random().nextInt();

	private static boolean isRunning = false;
	public static boolean isRunning() {
		return isRunning;
	}

	private void runAsForeground() {
		STOP_SERVICE = getString(R.string.stop_service);

		Intent notificationIntent = new Intent(this, PagerActivity.class);
		Intent disconnectNotificationIntent = new Intent(STOP_SERVICE);

		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		PendingIntent disconnectIntent = PendingIntent.getBroadcast(this, 0, disconnectNotificationIntent, 0);

		Notification notification = new NotificationCompat.Builder(this)
				.setSmallIcon(R.mipmap.ic_launcher)
				.setContentTitle("FotoCon")
				.setContentText("You are connected to a room")
				.setContentIntent(pendingIntent)
				.addAction(R.drawable.ic_close_black_24dp, "Leave room", disconnectIntent)
				.build();

		startForeground(NOTIFICATION_ID, notification);
	}


	@Override
	public int onStartCommand(final Intent intent, int flags, int startId) {

		runAsForeground();

		handler = new Handler();
		new Thread(new Runnable() {
			@Override
			public void run() {
				broadcastReceiver = new BroadcastReceiver() {
					@Override
					public void onReceive(Context context, Intent intent) {
						if (intent.getAction().equals(STOP_SERVICE)) {
							closeRoom();
						}
					}
				};
				registerReceiver(broadcastReceiver, new IntentFilter(STOP_SERVICE));

				binder = new ClientBinder();
				InetAddress host = (InetAddress)intent.getSerializableExtra("host");
				me = (Sender)intent.getSerializableExtra("me");
				int port = (Integer)intent.getSerializableExtra("port");

				try {
					Socket socket = new Socket(host, port);

					ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());

					SocketHolder socketHolder = new SocketHolder(
							socket,
							new ObjectInputStream(socket.getInputStream()),
							objectOutputStream
					);

					sync(socketHolder);
					isRunning = true;
					clientSocketComms = new ClientSocketComms(socketHolder);
					clientSocketComms.setMessageListener(new MessageListener() {
						@Override
						public void messageReceived(Message m) {
							persistMessage(m);
							broadcastMessage(m);
						}
					});
					clientSocketComms.setConnectionStatusListener(new ConnectionStatusListener() {
						@Override
						public void connectionDropped() {
							handler.post(new Runnable() {
								@Override
								public void run() {
									Toast.makeText(ClientService.this, "Disconnected from " + room.getRoomName(), Toast.LENGTH_SHORT).show();
									shutdown();
								}
							});
						}
					});
					clientSocketComms.setMemberStateChangeListener(new MemberStateChangeListener() {
						@Override
						public void memberJoined(Sender s) {
							room.addSender(s);
						}

						@Override
						public void memberLeft(Sender s) {
							room.removeSender(s);
						}
					});
					sendBroadcast(new Intent(getString(R.string.service_state_change)));
					clientSocketComms.startReceiving();
				} catch (Exception e) {}
			}
		}).start();
		return START_NOT_STICKY;
	}

	private void sync(final SocketHolder socketHolder) throws Exception {
		final ObjectOutputStream os = socketHolder.objectOutputStream;
		final ObjectInputStream is = socketHolder.objectInputStream;

		// send self to host
		os.writeObject(me);

		// receive id from host
		myId = String.valueOf(is.readObject());

		// synchronize with host
		room = (Room)is.readObject();
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	private void closeRoom() {
		if (clientSocketComms != null) {
			clientSocketComms.disconnect();
			clientSocketComms = null;
		}
	}


	private void persistMessage(Message m) {
		room.addMessage(m);
	}

	private void broadcastMessage(Message m) {
		binder.notifyListeners(m);
	}


	private void shutdown() {
		room.persist(this);
		unregisterReceiver(broadcastReceiver);
		sendBroadcast(new Intent(getString(R.string.service_state_change)));
		isRunning = false;
		stopSelf();
		stopForeground(true);
	}

	private ClientSocketComms clientSocketComms = null;

	public class ClientBinder extends ServiceBinder {
		public void sendMessage(Message m) {
			persistMessage(m);
			broadcastMessage(m);
			clientSocketComms.sendMessage(m);
		}
		public void disconnect() {
			closeRoom();
		}
		public Room getRoom() {
			return room;
		}
		public String getSenderId() {
			return myId;
		}

		public void registerMessageCallback(MessageListener l) {
			this.listener = l;
		}

		public void notifyListeners(Message m) {
			if (listener != null) {
				listener.messageReceived(m);
			}
		}
		public void unregisterMessageCallback() {
			listener = null;
		}

		private MessageListener listener;
	}
}
