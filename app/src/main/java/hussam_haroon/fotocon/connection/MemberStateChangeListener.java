package hussam_haroon.fotocon.connection;

import java.util.ArrayList;

import hussam_haroon.fotocon.shared.Sender;

/**
 * Created by M_har on 24-Apr-17.
 */

public interface MemberStateChangeListener {
	void memberJoined(Sender s);
	void memberLeft(Sender s);
}

