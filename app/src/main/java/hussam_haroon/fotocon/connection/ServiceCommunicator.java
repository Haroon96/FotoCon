package hussam_haroon.fotocon.connection;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import hussam_haroon.fotocon.activities.PagerActivity;
import hussam_haroon.fotocon.connection.client.ClientService;
import hussam_haroon.fotocon.connection.host.HostService;
import hussam_haroon.fotocon.message.Message;
import hussam_haroon.fotocon.shared.Room;

/**
 * Created by M_har on 28-Apr-17.
 */

public class ServiceCommunicator {

	private ServiceBinder binder;
	private ServiceConnection serviceConnection;
	private static MessageListener messageListener = null;
	private static ServiceCommunicator serviceCommunicator = null;

	private ServiceCommunicator(Context context, final Runnable callback) {
		serviceConnection = new ServiceConnection() {
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				binder = (ServiceBinder)service;
				binder.registerMessageCallback(messageListener);
				if (callback != null) {
					callback.run();
				}
			}
			@Override
			public void onServiceDisconnected(ComponentName name) {}
		};
		if (isClientServiceRunning()) {
			Intent intent = new Intent(context, ClientService.class);
			context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
		} else if (isHostServiceRunning()) {
			Intent intent = new Intent(context, HostService.class);
			context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
		} else if (callback != null) {
				callback.run();
		}
	}

	public static void init(Context context, Runnable callback) {
		serviceCommunicator = new ServiceCommunicator(context, callback);
	}
	public static void init(Context context) {
		serviceCommunicator = new ServiceCommunicator(context, null);
	}

	public static ServiceCommunicator getInstance() {
		return serviceCommunicator;
	}

	public static boolean isHostServiceRunning() {
		return HostService.isRunning();
	}
	public static boolean isClientServiceRunning() {
		return ClientService.isRunning();
	}
	public static boolean isConnected() {
		return isHostServiceRunning() || isClientServiceRunning();
	}

	public Room getRoom() {
		if (binder != null) {
			return binder.getRoom();
		}
		return null;
	}
	public void forwardMessage(Message m) {
		if (binder != null) {
			binder.sendMessage(m);
		}
	}
	public void disconnect() {
		if (binder != null) {
			binder.disconnect();
			binder = null;
		}
	}

	public static void registerMessageCallback(MessageListener listener) {
		messageListener = listener;
	}

	public void unregisterCallback() {
		binder.unregisterMessageCallback();
	}

	public String getSenderId() {
		return binder.getSenderId();
	}

	public boolean isServiceBound() {
		return binder != null;
	}

}
