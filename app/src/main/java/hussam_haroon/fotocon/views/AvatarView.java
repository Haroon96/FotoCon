package hussam_haroon.fotocon.views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import hussam_haroon.fotocon.R;

/**
 * Created by M_har on 28-Mar-17.
 */

public class AvatarView extends AppCompatImageView {

	static {
		fgPaint = new Paint();
		bgPaint = new Paint();
		blackStroke = new Paint();
	}
	{
		bgPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		bgPaint.setColor(getResources().getColor(R.color.colorAccent));
		fgPaint.setColor(Color.WHITE);
		blackStroke.setStyle(Paint.Style.STROKE);
		blackStroke.setColor(Color.WHITE);
		blackStroke.setStrokeWidth(8);
	}
	public AvatarView(Context context, AttributeSet attribs) {
		super(context, attribs);
		setScaleType(ScaleType.CENTER_INSIDE);
	}

	public void setData(String initials, Bitmap avatar) {
		setData(avatar);
		int indexOfSpace = initials.indexOf(' ');
		if (indexOfSpace == -1) {
			// only first name
			this.initials = initials.substring(0, 1).toUpperCase();
		} else {
			this.initials = initials.substring(0, 1).toUpperCase() + initials.substring(indexOfSpace + 1, indexOfSpace + 2).toUpperCase();
		}
	}

	public void setData(Bitmap avatar) {
		this.avatar = avatar;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// width = height
		float canvasDimension = getWidth();
		float radius = canvasDimension / 2;
		Path clipPath = new Path();

		fgPaint.setTextSize(canvasDimension / 2);

		clipPath.addCircle(radius, radius, radius, Path.Direction.CCW);

		canvas.clipPath(clipPath);

		if (avatar == null && initials == null) {
			// selection button
			canvas.drawCircle(radius, radius, radius, blackStroke);
			setImageResource(R.drawable.ic_add_a_photo_black_24dp);
			setImageTintList(ColorStateList.valueOf(Color.WHITE));
			super.onDraw(canvas);
			return;
		}
		canvas.drawRect(0, 0, canvasDimension, canvasDimension, bgPaint);
		if (avatar != null) {
			setImageBitmap(avatar);
			super.onDraw(canvas);
		} else {
			fgPaint.setTextAlign(Paint.Align.CENTER);
			float textHeight = (fgPaint.ascent() + fgPaint.descent());
			canvas.drawText(initials, (canvasDimension) / 2, (canvasDimension - textHeight) / 2, fgPaint);
		}
	}

	public Bitmap getAvatar() {
		return avatar;
	}

	private String initials = null;
	private Bitmap avatar = null;
	private static final Paint blackStroke;
	private static final Paint bgPaint;
	private static final Paint fgPaint;
}
