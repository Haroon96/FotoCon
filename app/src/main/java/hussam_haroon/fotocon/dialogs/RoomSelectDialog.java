package hussam_haroon.fotocon.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.connection.client.ClientNsd;
import hussam_haroon.fotocon.shared.RoomServiceAdapter;
import hussam_haroon.fotocon.shared.RoomServiceHolder;

/**
 * Created by M_har on 27-Apr-17.
 */

public class RoomSelectDialog extends DialogFragment implements ClientNsd.RoomFoundListener {

	private Activity activity;
	private Handler handler = new Handler();
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.activity = getActivity();
		roomAdapter = new RoomServiceAdapter(activity);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		View view = activity.getLayoutInflater().inflate(R.layout.dialog_room_select, null, false);

		ListView listView = (ListView)view.findViewById(R.id.room_list);

		listView.setAdapter(roomAdapter);

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				roomSelectionListener.selectedRoom(roomAdapter.getItem(position));
				roomAdapter.clear();
				dismiss();
			}
		});

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select a room to connect to");
		builder.setView(view);
		return builder.create();
	}


	@Override
	public void roomFound(final RoomServiceHolder room) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				roomAdapter.add(room);
				roomAdapter.notifyDataSetChanged();
			}
		});
	}

	@Override
	public void roomLost(final RoomServiceHolder room) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				roomAdapter.remove(room);
				roomAdapter.notifyDataSetChanged();
			}
		});
	}

	private RoomServiceAdapter roomAdapter;

	public void setRoomSelectionListener(RoomSelectionListener roomSelectionListener) {
		this.roomSelectionListener = roomSelectionListener;
	}

	public interface RoomSelectionListener {
		void selectedRoom(RoomServiceHolder roomServiceHolder);
	}
	private RoomSelectionListener roomSelectionListener;
}
