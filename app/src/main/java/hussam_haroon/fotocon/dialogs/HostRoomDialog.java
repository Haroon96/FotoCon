package hussam_haroon.fotocon.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

import hussam_haroon.fotocon.R;

/**
 * Created by M_har on 27-Apr-17.
 */

public class HostRoomDialog extends DialogFragment {
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Activity activity = getActivity();
		final Context context = activity;
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		view = activity.getLayoutInflater().inflate(R.layout.host_room_dialog, null, false);

		this.roomNameField = (EditText)view.findViewById(R.id.roomName);

		builder.setView(view);

		builder.setTitle("Enter room name to begin hosting")
				.setPositiveButton("Host", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						boolean ok = true;
						String roomName = roomNameField.getText().toString();
						if (ok) {
							hostRoomListener.startHosting(roomName);
						}
					}
				});
		return builder.create();
	}

	public void setHostRoomListener(HostRoomListener hostRoomListener) {
		this.hostRoomListener = hostRoomListener;
	}

	public interface HostRoomListener {
		void startHosting(String roomName);
	}

	private HostRoomListener hostRoomListener;

	private View view;
	private EditText roomNameField;
}
