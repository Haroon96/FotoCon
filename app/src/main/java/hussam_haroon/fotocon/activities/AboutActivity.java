package hussam_haroon.fotocon.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.shared.Profile;
import hussam_haroon.fotocon.views.AvatarView;

public class AboutActivity extends AppCompatActivity {


	private static final int GALLERY_REQUEST_CODE = 1001;

	private Button saveBtn;
	private EditText nameField;
	private AvatarView avatarView;

	private static final String TAG = "MainActivity";
	private AdView mAdView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);

		mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);

		setViews();
		setValues();
		setListeners();
	}

	private void setListeners() {
		avatarView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(intent, GALLERY_REQUEST_CODE);
			}
		});


		saveBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final String name = nameField.getText().toString();
				final Bitmap bitmap = avatarView.getAvatar();
				if (name.equals("")) {
					nameField.setError("Required");
					return;
				}

				nameField.setEnabled(false);
				saveBtn.setEnabled(false);
				avatarView.setEnabled(false);

				new AsyncTask<Void, Void, Void>() {
					@Override
					protected Void doInBackground(Void... params) {
						Profile.getInstance().getSelf().setName(name);
						Profile.getInstance().getSelf().setPicture(bitmap);
						Profile.save(AboutActivity.this);
						return null;
					}

					@Override
					protected void onPostExecute(Void aVoid) {
						Toast.makeText(AboutActivity.this, "Profile saved", Toast.LENGTH_SHORT).show();
					}
				}.execute(null, null, null);
			}
		});

	}

	private void setViews() {
		this.saveBtn = (Button)findViewById(R.id.saveBtn);
		this.nameField = (EditText)findViewById(R.id.username);
		this.avatarView = (AvatarView)findViewById(R.id.avatar);
	}

	private void setValues() {
		String name = Profile.getInstance().getSelf().getName();
		Bitmap bmp = Profile.getInstance().getSelf().getPicture();

		nameField.setText(name);
		avatarView.setData(bmp);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && requestCode == GALLERY_REQUEST_CODE) {
			try {
				Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
				avatarView.setData(bitmap);
				avatarView.refreshDrawableState();
			} catch (Exception e) {
			}
		}
	}

}
