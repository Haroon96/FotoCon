package hussam_haroon.fotocon.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.activities.utilities.Helpers;
import hussam_haroon.fotocon.message.Image;
import hussam_haroon.fotocon.message.Message;
import hussam_haroon.fotocon.message.MessageCollection;
import hussam_haroon.fotocon.message.System;
import hussam_haroon.fotocon.message.Text;
import hussam_haroon.fotocon.shared.Room;
import hussam_haroon.fotocon.shared.RoomCollection;
import hussam_haroon.fotocon.shared.Sender;

public class RoomThreadActivity extends AppCompatActivity {

	Room room;
	MessageCollection messages;
	ArrayList<View> views;

	private boolean changesMade = false;

	LinearLayout main;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_room_thread);
		main = (LinearLayout) findViewById(R.id.lyt_thread_view_main);

		Intent i = getIntent();
		String roomId = (String)i.getSerializableExtra("roomId");

		this.room = RoomCollection.getInstance().getRoom(roomId);

		room.getMessages().load(roomId, this);

		messages = room.getMessages();
		views = new ArrayList<>();

		populateViews();
	}

	void populateViews(){
		main.removeAllViews();
		for (Message m : messages.messages){
			View v = m.getView(this);
			views.add(v);
			main.addView(v);
			registerForContextMenu(v);
			v.setTag(m);
		}
	}

	void addViews(){
		for (View v : views){
			main.addView(v);
		}
	}

	// ------------------------------ Context Menus --------------------------------------
	View called_context;

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		boolean isImage = ((Message)v.getTag()).isImage();
		boolean systemMsg = ((Message)v.getTag()).getSenderName().equals(System.SYSTEM_SENDER);
		if (isImage){
			getMenuInflater().inflate(R.menu.ctx_image_long, menu);
			called_context = v;
		}
		if (!isImage && !systemMsg){
			getMenuInflater().inflate(R.menu.ctx_text_long, menu);
			called_context = v;
		}
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		Message m = (Message) called_context.getTag();
		if (item.getItemId() == R.id.mnu_copy){
			String cp = m.toString();
			setClipboard(cp);
		}

		if (item.getItemId() == R.id.mnu_del){
			View view = null;
			for(View v : views){
				if (((Message)v.getTag()).getId() == m.getId()){
					view = v;
				}
			}
			views.remove(view);
			messages.messages.remove(m);
			main.removeAllViews();
			addViews();
			changesMade = true;
		}

		if (item.getItemId() == R.id.mnu_save){
			try{
				saveToGallery(((Image)m).getBitmap(), "" + m.getId());
			}
			catch (Exception e){
				Toast.makeText(this, "Unable to save to gallery", Toast.LENGTH_SHORT).show();
			}
		}
		return super.onContextItemSelected(item);
	}

	// ----------------------------- Saving to Gallery --------------------------------
	private void refreshGallery(File file) {
		Intent mediaScanIntent = new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		mediaScanIntent.setData(Uri.fromFile(file));
		this.sendBroadcast(mediaScanIntent);
	}

	public void saveToGallery(Bitmap b, String name){
		File myDir = new File("/sdcard/FotoCon");
		myDir.mkdirs();

		name = "Image-" + name + ".jpg";
		File file = new File (myDir, name);

		if (file.exists ()) file.delete ();
		try {
			FileOutputStream out = new FileOutputStream(file);
			b.compress(Bitmap.CompressFormat.JPEG, 90, out);
			out.flush();
			out.close();
			refreshGallery(file);
			Toast.makeText(this, "Saved to gallery", Toast.LENGTH_SHORT).show();

		} catch (Exception e) {
			Toast.makeText(this, "Could not Save", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		room.getMessages().messages.clear();
	}

	@Override
	public void onBackPressed() {
		if (changesMade) {
			room.remove();
			room.persist(this);
		}
		super.onBackPressed();
	}

	// ------------------------------- Clipboard ----------------------------------------
	private void setClipboard(String text) {
		android.content.ClipboardManager clipboard = (android.content.ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE);
		android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
		clipboard.setPrimaryClip(clip);
		Toast.makeText(this, "Copied to Clipboard", Toast.LENGTH_SHORT).show();
	}
}
