package hussam_haroon.fotocon.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.File;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.activities.utilities.Helpers;
import hussam_haroon.fotocon.activities.utilities.SwipeDetector;
import hussam_haroon.fotocon.connection.ServiceCommunicator;
import hussam_haroon.fotocon.shared.Profile;
import hussam_haroon.fotocon.shared.RoomCollection;
import hussam_haroon.fotocon.views.AvatarView;


public class MainActivity extends AppCompatActivity {

	private FirebaseAnalytics mFirebaseAnalytics;
	private static final int GALLERY_REQUEST_CODE = 1002;
	private static final int PERMISSIONS_REQUEST_CODE = 5010;

	private static final int expectedCallbacks = 3;
	private static int receivedCallbacks = 0;

	private boolean permissionsGranted = false;

	private Runnable callback = new Runnable() {
		@Override
		public void run() {
			receivedCallbacks++;
			check();
		}
	};

	private void check() {
		if (receivedCallbacks == expectedCallbacks &&  permissionsGranted) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					launchApp();
				}
			});
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		switch (requestCode) {
			case PERMISSIONS_REQUEST_CODE:
				checkPermissions();
				break;
			default:
				super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}

	private void checkPermissions() {
		int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
		int storagePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

		String[] permissions = null;

		if (cameraPermission != PackageManager.PERMISSION_GRANTED && storagePermission != PackageManager.PERMISSION_GRANTED) {
			// ask both permissions
			permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
		} else if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
			permissions = new String[]{Manifest.permission.CAMERA};
		} else if (storagePermission != PackageManager.PERMISSION_GRANTED) {
			permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
		}

		if (permissions != null) {
			ActivityCompat.requestPermissions(this, permissions, PERMISSIONS_REQUEST_CODE);
		} else {
			permissionsGranted = true;
			check();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		receivedCallbacks = 0;

		checkPermissions();

		setContentView(R.layout.splash);

		mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

		if (Profile.exists(this)) {
			Profile.load(this, callback);
			initDependencies();
		} else {
			setContentView(R.layout.activity_main);
			setViews();
			setListeners();
		}
	}

	private void initDependencies() {
		ServiceCommunicator.init(this, callback);
		RoomCollection.init(this);
		RoomCollection.getInstance().load(callback);
	}

	private void setListeners() {
		saveBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final String name = nameField.getText().toString();

				if (name.equals("")) {
					nameField.setError("Required");
					return;
				}

				nameField.setEnabled(false);
				saveBtn.setEnabled(false);
				avatarView.setEnabled(false);

				new Thread(new Runnable() {
					@Override
					public void run() {
						Profile.init(name);
						Profile.getInstance().getSelf().setPicture(avatarView.getAvatar());
						Profile.save(MainActivity.this, callback);
						initDependencies();
					}
				}).start();
			}
		});
		avatarView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(intent, GALLERY_REQUEST_CODE);
			}
		});
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && requestCode == GALLERY_REQUEST_CODE) {
			try {
				Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
				avatarView.setData(bitmap);
				avatarView.refreshDrawableState();
			} catch (Exception e) {
			}
		}
	}

	private void setViews() {
		this.saveBtn = (Button)findViewById(R.id.saveBtn);
		this.nameField = (EditText)findViewById(R.id.username);
		this.avatarView = (AvatarView)findViewById(R.id.avatar);
	}

	private void launchApp() {
		Intent i = new Intent(MainActivity.this, PagerActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(i);
		overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
	}


	private Button saveBtn;
	private EditText nameField;
	private AvatarView avatarView;
}