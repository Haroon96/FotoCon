package hussam_haroon.fotocon.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.connection.MessageListener;
import hussam_haroon.fotocon.connection.ServiceCommunicator;
import hussam_haroon.fotocon.message.Message;

public class PagerActivity extends AppCompatActivity {
	private SectionsPagerAdapter mSectionsPagerAdapter;
	private ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_pager);

		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.container);
		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setCurrentItem(SectionsPagerAdapter.CAMERA_FRAGMENT_INDEX);


		ServiceCommunicator.registerMessageCallback(new MessageListener() {
			@Override
			public void messageReceived(final Message m) {
				threadViewFragment.populateView(m);
			}
		});
	}


	public final MenuFragment menuFragment = new MenuFragment();
	public final CameraFragment cameraFragment = new CameraFragment();
	public final ThreadViewFragment threadViewFragment = new ThreadViewFragment();


	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public static final int MENU_FRAGMENT_INDEX = 0;
		public static final int CAMERA_FRAGMENT_INDEX = 1;
		public static final int THREAD_FRAGMENT_INDEX = 2;

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			if (position == MENU_FRAGMENT_INDEX) {
				return menuFragment;
			} else if (position == CAMERA_FRAGMENT_INDEX) {
				return cameraFragment;
			} else if (position == THREAD_FRAGMENT_INDEX) {
				return threadViewFragment;
			}
			return null;
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
				case 0:
					return "Menu";
				case 1:
					return "Camera";
				case 2:
					return "Thread";
			}
			return null;
		}
	}

	// ------------------------------ Context Menus --------------------------------------


	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		threadViewFragment.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		threadViewFragment.onContextItemSelected(item);
		return super.onContextItemSelected(item);
	}

}
