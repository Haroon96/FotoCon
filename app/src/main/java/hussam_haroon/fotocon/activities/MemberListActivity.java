package hussam_haroon.fotocon.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.activities.utilities.Helpers;
import hussam_haroon.fotocon.connection.ServiceCommunicator;
import hussam_haroon.fotocon.shared.Sender;
import hussam_haroon.fotocon.shared.SenderAdapter;

/**
 * Created by M_har on 01-May-17.
 */

public class MemberListActivity extends AppCompatActivity {

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_member_list);

		Helpers.applyWallpaper(findViewById(R.id.parent));

		ArrayList<Sender> members = (ArrayList<Sender>)getIntent().getSerializableExtra("senders");

		SenderAdapter adapter = new SenderAdapter(this);
		adapter.addAll(members);

		((GridView)findViewById(R.id.listview)).setAdapter(adapter);
	}
}
