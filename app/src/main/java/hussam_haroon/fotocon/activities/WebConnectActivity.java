package hussam_haroon.fotocon.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.io.ObjectOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.message.Image;
import hussam_haroon.fotocon.shared.Room;
import hussam_haroon.fotocon.shared.RoomCollection;

public class WebConnectActivity extends AppCompatActivity {

	private EditText hostIpField;
	private Room room;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_connect);

		this.room = RoomCollection.getInstance().getRoom((String)getIntent().getSerializableExtra("roomId"));
		this.hostIpField = (EditText)findViewById(R.id.host_ip);
		room.getMessages().load(room.getId(), this);
	}

	public void send(View v) {
		if (hostIpField.getText().equals("")) {
			hostIpField.setError("Required");
			return;
		}

		String url = "http://" + hostIpField.getText().toString() + ":8080/FotoCon";

		new WebConnectionAssistant(url, room).start();

	}


	private static class WebConnectionAssistant extends Thread {
		private String url;
		private Room room;
		WebConnectionAssistant(String url, Room room) {
			this.url = url;
			this.room = room;
		}

		@Override
		public void run() {
			try {
				URL Url = new URL(url);
				URLConnection urlConnection = Url.openConnection();
				urlConnection.setDoOutput(true);
				ObjectOutputStream stream = new ObjectOutputStream(urlConnection.getOutputStream());

				stream.writeObject(room.getRoomName());
				for (int i = 0; i < room.getMessages().size(); ++i) {
					if (room.getMessages().messages.get(i).isImage()) {
						stream.writeInt(1);
						stream.writeObject(room.getMessages().messages.get(i).getData());
					}
				}
				stream.writeInt(-1);
				stream.close();

				int status = urlConnection.getInputStream().read();

				if (status == 200) {

				}

			} catch (Exception e) {}
		}
	}
}

