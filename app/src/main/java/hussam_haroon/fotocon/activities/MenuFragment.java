package hussam_haroon.fotocon.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import java.net.InetAddress;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.activities.utilities.Helpers;
import hussam_haroon.fotocon.connection.ServiceCommunicator;
import hussam_haroon.fotocon.connection.client.ClientNsd;
import hussam_haroon.fotocon.connection.client.ClientService;
import hussam_haroon.fotocon.connection.host.HostService;
import hussam_haroon.fotocon.dialogs.HostRoomDialog;
import hussam_haroon.fotocon.dialogs.RoomSelectDialog;
import hussam_haroon.fotocon.message.Text;
import hussam_haroon.fotocon.shared.Room;
import hussam_haroon.fotocon.shared.RoomAdapter;
import hussam_haroon.fotocon.shared.RoomServiceAdapter;
import hussam_haroon.fotocon.shared.Profile;
import hussam_haroon.fotocon.shared.RoomCollection;
import hussam_haroon.fotocon.shared.RoomServiceHolder;
import hussam_haroon.fotocon.shared.Sender;
import hussam_haroon.fotocon.views.AvatarView;

/**
 * Created by M_har on 24-Apr-17.
 */

public class MenuFragment extends Fragment implements RoomSelectDialog.RoomSelectionListener, HostRoomDialog.HostRoomListener {

	private Activity activity;
	private Context context;
	private View view;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_left, container, false);

		// AD DEBUG
		ImageButton b = (ImageButton) view.findViewById(R.id.settingsBtn);
		b.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(activity, AboutActivity.class);
				activity.startActivity(i);
			}
		});
		return view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		this.activity = getActivity();
		this.context = activity;
		initViews();
	}

	private void initViews() {

		String name = Profile.getInstance().getSelf().getName();
		Bitmap bmp = Profile.getInstance().getSelf().getPicture();

		((AvatarView)view.findViewById(R.id.avatar)).setData(name, bmp);
		((TextView)view.findViewById(R.id.name_field)).setText(name.toUpperCase());

		this.listView = (ListView)view.findViewById(R.id.thread_list);

		view.findViewById(R.id.searchBtn).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				searchRooms();
			}
		});

		view.findViewById(R.id.hostBtn).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				hostRoom();
			}
		});

		setWallpaper();
		populateRoomList();
	}

	public void setWallpaper(){
		Helpers.applyWallpaper(view.findViewById(R.id.parent));
	}


	private void populateRoomList() {
		final RoomAdapter adapter = new RoomAdapter(activity);
		adapter.addAll(RoomCollection.getInstance().getRooms());
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Room room = adapter.getItem(position);
				Intent intent = new Intent(activity, RoomThreadActivity.class);
				intent.putExtra("roomId", room.getId());
				activity.startActivity(intent);
			}
		});
		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(activity, WebConnectActivity.class);
				intent.putExtra("roomId", adapter.getItem(position).getId());
				activity.startActivity(intent);
				return true;
			}
		});
	}


	// ---------------------------------------
	// on click listeners for buttons
	// ---------------------------------------
	
	private void hostRoom() {
		if (ServiceCommunicator.isConnected()) {
			Snackbar.make(view, "Already connected to a room", Snackbar.LENGTH_SHORT).show();
			return;
		}
		HostRoomDialog hostRoomDialog = new HostRoomDialog();

		hostRoomDialog.setHostRoomListener(this);

		hostRoomDialog.show(getFragmentManager(), "Room Host");
	}

	private void searchRooms() {
		if (ServiceCommunicator.isConnected()) {
			Snackbar.make(view, "Already connected to a room", Snackbar.LENGTH_SHORT).show();
			return;
		}
		RoomSelectDialog roomSelectDialog = new RoomSelectDialog();
		this.clientNsd = new ClientNsd(context);
		clientNsd.setRoomFoundListener(roomSelectDialog);
		clientNsd.startSearch();
		roomSelectDialog.setRoomSelectionListener(this);
		roomSelectDialog.show(getFragmentManager(), "Room Select");
	}
	// ---------------------------------------


	// ---------------------------------------
	// implemented interfaces
	// ---------------------------------------
	@Override
	public void selectedRoom(RoomServiceHolder roomServiceHolder) {
		// connect to room here

		this.clientNsd.setServiceResolvedListener(new ClientNsd.ServiceResolvedListener() {
			@Override
			public void params(InetAddress host, int port) {
				Intent intent = new Intent(activity, ClientService.class);
				intent.putExtra("host", host);
				intent.putExtra("port", new Integer(port));
				intent.putExtra("me", Profile.getInstance().getSelf());
				activity.startService(intent);
				new Thread(new Runnable() {
					@Override
					public void run() {
						while (ServiceCommunicator.isConnected() == false) {}
						ServiceCommunicator.init(context);
					}
				}).start();
			}
		});

		this.clientNsd.connect(roomServiceHolder);
	}

	@Override
	public void startHosting(String roomName) {
		// start hosting here
		Intent intent = new Intent(activity, HostService.class);
		intent.putExtra("roomName", roomName);
		intent.putExtra("hostName", Profile.getInstance().getSelf().getName());
		intent.putExtra("Me", Profile.getInstance().getSelf());
		activity.startService(intent);
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (ServiceCommunicator.isConnected() == false) {}
				ServiceCommunicator.init(context);
			}
		}).start();
	}
	// ---------------------------------------

	private ListView listView;
	private ClientNsd clientNsd;
}
