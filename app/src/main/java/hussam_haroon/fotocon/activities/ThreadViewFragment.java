package hussam_haroon.fotocon.activities;

import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.UUID;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.activities.utilities.Helpers;
import hussam_haroon.fotocon.activities.utilities.SwipeDetector;
import hussam_haroon.fotocon.connection.MessageListener;
import hussam_haroon.fotocon.connection.ServiceCommunicator;
import hussam_haroon.fotocon.message.Image;
import hussam_haroon.fotocon.message.Message;
import hussam_haroon.fotocon.message.MessageCollection;
import hussam_haroon.fotocon.message.System;
import hussam_haroon.fotocon.message.Text;
import hussam_haroon.fotocon.shared.Profile;
import hussam_haroon.fotocon.shared.Room;

/**
 * Created by M_har on 17-Apr-17.
 */

public class ThreadViewFragment extends Fragment {

	private static String SERVICE_STATE_CHANGE;

	MessageCollection messages;
	ArrayList<View> views;
	private Room room;

	LinearLayout main;
	LinearLayout secondary;
	LinearLayout menu;
	TextView errorMessage;
	View called_context;
	private View view;
	private Activity activity;
	private ImageButton sendBtn;
	private TextView roomNameField;
	private ImageView infoBtn;
	private EditText messageField;


	private MessageBroadcastReceiver messageBroadcastReceiver;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return view = inflater.inflate(R.layout.activity_thread_view, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		this.activity = getActivity();

		views = new ArrayList<>();

		sendBtn = (ImageButton)view.findViewById(R.id.sendBtn);
		messageField = (EditText)view.findViewById(R.id.message_field);
		main = (LinearLayout)view.findViewById(R.id.lyt_thread_view_main);
		secondary = (LinearLayout)view.findViewById(R.id.secondary);
		menu = (LinearLayout)view.findViewById(R.id.menu);
		errorMessage = (TextView)view.findViewById(R.id.errorMsg);
		roomNameField = (TextView)view.findViewById(R.id.room_name);
		infoBtn = (ImageView)view.findViewById(R.id.infoBtn);
		setWallpaper();


		SERVICE_STATE_CHANGE = getString(R.string.service_state_change);

		messageBroadcastReceiver = new MessageBroadcastReceiver();

		IntentFilter intentFilter = new IntentFilter(SERVICE_STATE_CHANGE);
		activity.registerReceiver(messageBroadcastReceiver, intentFilter);


		infoBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(activity, MemberListActivity.class);
				intent.putExtra("senders", ServiceCommunicator.getInstance().getRoom().getSenders());
				activity.startActivity(intent);
			}
		});
		sendBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Message m = new Text(Profile.getInstance().getSelf().getName(),
						Helpers.getDate(),
						messageField.getText().toString()
				);
				ServiceCommunicator.getInstance().forwardMessage(m);
				messageField.setText("");
			}
		});

		checkRoom();
	}

	public void checkRoom() {
		if (!ServiceCommunicator.isConnected()) {
			main.setVisibility(View.GONE);
			secondary.setVisibility(View.GONE);
			menu.setVisibility(View.GONE);
			errorMessage.setVisibility(View.VISIBLE);
		} else {
			this.room = ServiceCommunicator.getInstance().getRoom();
			main.setVisibility(View.VISIBLE);
			secondary.setVisibility(View.VISIBLE);
			menu.setVisibility(View.VISIBLE);
			errorMessage.setVisibility(View.GONE);
			roomNameField.setText(room.getRoomName());
			messages = room.getMessages();
			populateViews();
		}
	}

	private void populateViews(){
		for (Message m : messages.messages){
			populateView(m);
		}
	}
	public void populateView(final Message m){
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				new AddViewTask(activity, m).execute(m);
			}
		});
	}


	public void addViews(){
		for (View v : views){
			main.addView(v);
			registerForContextMenu(v);
		}
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	// ------------------------------- Action Menu ------------------------------------------

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
		menuInflater.inflate(R.menu.act_thread_menu, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.mnu_change_background){
			changeWallpaper();
		}
		return super.onOptionsItemSelected(item);
	}

	// ------------------------------ Context Menus --------------------------------------
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		boolean isImage = ((Message)v.getTag()).isImage();
		boolean systemMsg = ((Message)v.getTag()).getSenderName().equals(System.SYSTEM_SENDER);
		if (isImage){
			activity.getMenuInflater().inflate(R.menu.ctx_image_long, menu);
			called_context = v;
		}
		if (!isImage && !systemMsg){
			activity.getMenuInflater().inflate(R.menu.ctx_text_long, menu);
			called_context = v;
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		Message m = (Message) called_context.getTag();
		if (item.getItemId() == R.id.mnu_copy){
			String cp = m.toString();
			setClipboard(cp);
		}

		if (item.getItemId() == R.id.mnu_del){
			View view = null;
			for(View v : views){
				if (((Message)v.getTag()).getId() == m.getId()){
					view = v;
				}
			}

			//messages.messages.remove(m);

			// Haroon remove View view from the LinearLayout main
			/*
			views.remove(view);
			main.removeAllViews();
			addViews();
			*/
		}

		if (item.getItemId() == R.id.mnu_save){
			final Message fm = m;
			new Thread(new Runnable() {
				@Override
				public void run() {
					saveToGallery(((Image)fm).getBitmap(), "FotoCon-Image-" + fm.getDate());
				}
			}).start();
		}
		return false;
	}

	// ----------------------------- Saving to Gallery --------------------------------
	private void refreshGallery(File file) {
		Intent mediaScanIntent = new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		mediaScanIntent.setData(Uri.fromFile(file));
		activity.sendBroadcast(mediaScanIntent);
	}

	public void saveToGallery(final Bitmap b, String name){
		File myDir = new File("/sdcard/FotoCon");
		myDir.mkdirs();

		name = name + ".jpg";

		final File file = new File (myDir, name);

		if (file.exists ()) file.delete ();

		try {
			FileOutputStream out = new FileOutputStream(file);
			b.compress(Bitmap.CompressFormat.JPEG, 90, out);
			out.flush();
			out.close();
			refreshGallery(file);
			Toast.makeText(activity, "Saved to gallery", Toast.LENGTH_SHORT).show();

		} catch (Exception e) {
			Toast.makeText(activity, "Could not Save", Toast.LENGTH_SHORT).show();
		}
	}

	// -------------------- Changing Wallpaper ----------------------

	private Bitmap getBitmapFromUri(Uri uri) throws IOException {
		ParcelFileDescriptor parcelFileDescriptor =
				activity.getContentResolver().openFileDescriptor(uri, "r");
		FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
		Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
		parcelFileDescriptor.close();
		return image;
	}

	public void getImageFromGallery(){
		Intent gallery = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(gallery, 1);
	}

	public void changeWallpaper(){
		getImageFromGallery();
	}

	public void setWallpaper(){
		Helpers.applyWallpaper(view.findViewById(R.id.lyt_thread_view_parent));
	}

	// ------------------------------- Clipboard ----------------------------------------

	private void setClipboard(String text) {
		android.content.ClipboardManager clipboard = (android.content.ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
		android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
		clipboard.setPrimaryClip(clip);
		Toast.makeText(activity, "Copied to Clipboard", Toast.LENGTH_SHORT).show();
	}

	private class AddViewTask extends AsyncTask<Message, Void, View> {

		private Context ctx;
		private Activity act;
		private Message m;

		AddViewTask(Activity ctx, Message m) {
			this.ctx = ctx;
			this.act = ctx;
			this.m = m;
		}

		@Override
		protected View doInBackground(Message... params) {
			View view = params[0].getView(ctx);
			return params[0].getView(ctx);
		}

		@Override
		protected void onPostExecute(View v) {
			views.add(v);
			v.setTag(m);
			act.registerForContextMenu(v);
			main.addView(v);
			registerForContextMenu(v);
		}
	}


	private class MessageBroadcastReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(SERVICE_STATE_CHANGE)) {
				checkRoom();
			}
		}
	}

}
