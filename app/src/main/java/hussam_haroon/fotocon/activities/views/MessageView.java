package hussam_haroon.fotocon.activities.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;
import hussam_haroon.fotocon.R;
/**
 * Created by M_har on 12-Apr-17.
 */

public class MessageView extends LinearLayout {

	public MessageView(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
		this.context = context;
		initLayout();
	}

	public void receivedMessage(String message, String sender, String date) {
		LayoutParams innerLayoutParams = new LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
		innerLayout.setLayoutParams(innerLayoutParams);

		Space space = new Space(context);
		LayoutParams spaceParams = new LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
		space.setLayoutParams(spaceParams);

		innerLayoutParams.weight = 0.7f;
		spaceParams.weight = 0.3f;

		LinearLayout[] layout = new LinearLayout[3];
		LayoutParams fieldParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


		layout[0] = new LinearLayout(context);
		layout[0].setLayoutParams(fieldParams);
		layout[1] = new LinearLayout(context);
		layout[1].setLayoutParams(fieldParams);
		layout[2] = new LinearLayout(context);
		layout[2].setLayoutParams(fieldParams);


		layout[0].addView(senderField);
		layout[1].addView(messageField);
		layout[2].addView(dateField);

		layout[2].setHorizontalGravity(Gravity.RIGHT);

		innerLayout.addView(layout[0]);
		innerLayout.addView(layout[1]);
		innerLayout.addView(layout[2]);

		innerLayout.setBackground(context.getDrawable(R.drawable.msg_2_bg));

		addView(innerLayout);
		addView(space);

		int color = Color.argb(120, 0, 0, 0);
		senderField.setTextColor(color);
		messageField.setTextColor(Color.BLACK);
		dateField.setTextColor(color);

		senderField.setTextSize(12);
		messageField.setTextSize(16);
		dateField.setTextSize(12);

		senderField.setText(sender);
		messageField.setText(message);
		dateField.setText(date);
	}

	public void receivedImage(Bitmap image, String sender, String date) {
		LayoutParams innerLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		innerLayout.setLayoutParams(innerLayoutParams);

		innerLayoutParams.gravity = Gravity.CENTER;

		LayoutParams textParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		LayoutParams imgParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dipToPx(200));


		int color = Color.argb(120, 0, 0, 0);
		TextView senderView = new TextView(context);
		senderView.setLayoutParams(textParams);
		senderView.setText(sender);
		senderView.setTextAlignment(TEXT_ALIGNMENT_CENTER);
		senderView.setPadding(0, 0, 0, dipToPx(5));
		senderView.setTextColor(color);

		TextView dateView = new TextView(context);
		dateView.setLayoutParams(textParams);
		dateView.setText(date);
		dateView.setTextAlignment(TEXT_ALIGNMENT_CENTER);
		dateView.setPadding(0, dipToPx(5), 0, 0);
		dateView.setTextColor(color);


		ImageView imgView = new ImageView(context);
		imgView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		imgView.setLayoutParams(imgParams);
		imgView.setImageBitmap(image);
		imgView.setBackgroundColor(Color.BLACK);

		innerLayout.setBackground(context.getDrawable(R.drawable.msg_2_bg));

		innerLayout.addView(senderView);
		innerLayout.addView(imgView);
		innerLayout.addView(dateView);
		addView(innerLayout);
	}

	public void sentImage(Bitmap image, String date) {
		LayoutParams innerLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		innerLayout.setLayoutParams(innerLayoutParams);

		innerLayoutParams.gravity = Gravity.CENTER;

		LayoutParams textParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		LayoutParams imgParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dipToPx(200));


		TextView textView = new TextView(context);
		textView.setLayoutParams(textParams);
		textView.setText(date);
		textView.setTextAlignment(TEXT_ALIGNMENT_CENTER);
		textView.setPadding(0, dipToPx(5), 0, 0);
		textView.setTextColor(Color.argb(120, 255, 255, 255));

		ImageView imgView = new ImageView(context);
		imgView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		imgView.setLayoutParams(imgParams);
		imgView.setImageBitmap(image);
		imgView.setBackgroundColor(Color.BLACK);

		innerLayout.setBackground(context.getDrawable(R.drawable.msg_bg));

		innerLayout.addView(imgView);
		innerLayout.addView(textView);
		addView(innerLayout);
	}


	public void sentMessage(String message, String date) {
		LayoutParams innerLayoutParams = new LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
		innerLayout.setLayoutParams(innerLayoutParams);

		Space space = new Space(context);
		LayoutParams spaceParams = new LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
		space.setLayoutParams(spaceParams);

		innerLayoutParams.weight = 0.7f;
		spaceParams.weight = 0.3f;

		LinearLayout[] layout = new LinearLayout[3];
		LayoutParams fieldParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


		layout[0] = new LinearLayout(context);
		layout[0].setLayoutParams(fieldParams);
		layout[1] = new LinearLayout(context);
		layout[1].setLayoutParams(fieldParams);


		layout[0].addView(messageField);
		layout[1].addView(dateField);


		layout[1].setHorizontalGravity(Gravity.RIGHT);

		innerLayout.addView(layout[0]);
		innerLayout.addView(layout[1]);

		innerLayout.setBackground(context.getDrawable(R.drawable.msg_bg));

		addView(space);
		addView(innerLayout);

		int color = Color.argb(120, 255, 255, 255);
		senderField.setTextColor(color);
		messageField.setTextColor(Color.WHITE);
		dateField.setTextColor(color);

		senderField.setTextSize(12);
		messageField.setTextSize(16);
		dateField.setTextSize(12);

		messageField.setText(message);
		dateField.setText(date);
	}

	public void receivedSystemMessage(String message) {
		LayoutParams innerLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		innerLayout.setLayoutParams(innerLayoutParams);
		innerLayout.addView(messageField);
		messageField.setText(message);
		messageField.setTextAlignment(TEXT_ALIGNMENT_CENTER);
		innerLayout.setHorizontalGravity(Gravity.CENTER);
	}

	private void initLayout() {
		messageField = new TextView(context);
		senderField = new TextView(context);
		dateField = new TextView(context);
		innerLayout = new LinearLayout(context);
		innerLayout.setOrientation(LinearLayout.VERTICAL);
		innerLayout.setPadding(dipToPx(10), dipToPx(10), dipToPx(10), dipToPx(10));
		setPadding(dipToPx(5),dipToPx(5),dipToPx(5),dipToPx(5));
	}

	private int dipToPx(int value) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, context.getResources().getDisplayMetrics());
	}

	private Context context;
	private LinearLayout innerLayout;
	private TextView messageField;
	private TextView senderField;
	private TextView dateField;
}
