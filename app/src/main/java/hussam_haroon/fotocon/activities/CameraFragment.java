package hussam_haroon.fotocon.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.Camera;
import android.net.Uri;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.activities.utilities.Helpers;
import hussam_haroon.fotocon.activities.utilities.SwipeDetector;
import hussam_haroon.fotocon.camera.Preview;
import hussam_haroon.fotocon.connection.ServiceCommunicator;
import hussam_haroon.fotocon.message.Image;
import hussam_haroon.fotocon.message.Message;
import hussam_haroon.fotocon.shared.Profile;

import static android.R.attr.width;
import static android.support.v7.appcompat.R.attr.height;

/**
 * Created by M_har on 17-Apr-17.
 */

public class CameraFragment extends Fragment {
	private static final String TAG = "CameraActivity";
	private static final int FRONT = 1;
	private static final int REAR = 0;
	private static final int IMAGE_LOADED = 1;


	private View view;

	Preview preview;
	Button buttonClick;
	Camera camera;
	Activity act;
	Context ctx;
	FrameLayout fl;
	int cameraId;
	ImageView imgTest;

	private boolean flashOn = false;
	ArrayList<View> views = new ArrayList<>();

	private ImageView flashBtn, rotateBtn, galleryBtn, msgBtn, shutterBtn;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return view = inflater.inflate(R.layout.activity_camera_main, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		ctx = act = getActivity();
		cameraId = REAR;

		flashBtn = (ImageView)view.findViewById(R.id.toggle_flash);
		shutterBtn = (ImageView)view.findViewById(R.id.btn_snap);
		galleryBtn = (ImageView)view.findViewById(R.id.btn_swap);
		msgBtn = (ImageView)view.findViewById(R.id.sendText);
		rotateBtn = (ImageView)view.findViewById(R.id.rotate_camera);

		views.add(view.findViewById(R.id.lyt_cam2));
		views.add(view.findViewById(R.id.lyt_cam));
		views.add(view.findViewById(R.id.sp1));
		views.add(galleryBtn);
		views.add(view.findViewById(R.id.sp2));
		views.add(shutterBtn);
		views.add(view.findViewById(R.id.sp3));
		views.add(msgBtn);
		views.add(view.findViewById(R.id.sp4));
		views.add(view.findViewById(R.id.sp11));
		views.add(flashBtn);
		views.add(view.findViewById(R.id.sp12));
		views.add(rotateBtn);
		views.add(view.findViewById(R.id.sp13));

		preview = new Preview(act, (SurfaceView)view.findViewById(R.id.surfaceView));
		preview.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT));

		fl = (FrameLayout) view.findViewById(R.id.layout);
		fl.addView(preview);


		// buttons
		flashBtn.setOnTouchListener(focusTintChanger);
		rotateBtn.setOnTouchListener(focusTintChanger);
		galleryBtn.setOnTouchListener(focusTintChanger);
		msgBtn.setOnTouchListener(focusTintChanger);
		// shutter button
		shutterBtn.setOnTouchListener(new View.OnTouchListener(){
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch(event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						((ImageView)v).setImageResource(R.drawable.shutter_pressed);
						break;
					case MotionEvent.ACTION_UP:
						((ImageView)v).setImageResource(R.drawable.shutter_idle);
						v.callOnClick();
						break;
				}
				return true;
			}
		});


		rotateBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				toggleCamera();
			}
		});
		flashBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				toggleFlash();
			}
		});
		galleryBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

			}
		});
		msgBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

			}
		});
		shutterBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				camera.takePicture(null, null, jpegCallback);
			}
		});

		preview.setKeepScreenOn(true);
		//initViews();
		//views.add(imgTest);
		flashOn = false;
	}

	public void test(View v) {
		Toast.makeText(act, "snap", Toast.LENGTH_SHORT).show();
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == IMAGE_LOADED && resultCode == Activity.RESULT_OK && null != data ){
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = act.getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String picturePath = cursor.getString(columnIndex);
			cursor.close();

			/*
			ImageView imageView = (ImageView) findViewById(R.id.img_cam);

			Bitmap bmp = null;
			try {
				bmp = getBitmapFromUri(selectedImage);
			} catch (IOException e) {
				e.printStackTrace();
			}
			imageView.setImageBitmap(bmp);*/
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		//TODO test
		stopCamera();
		showCamera();
	}

	@Override
	public void onResume() {
		super.onResume();
		int numCams = Camera.getNumberOfCameras();
		if(numCams > 0){
			try{
				camera = Camera.open(cameraId);
				camera.startPreview();
				preview.setCamera(camera);
			} catch (RuntimeException ex){
			}
		}
		bringToFront();
	}

	@Override
	public void onPause() {
		if(camera != null) {
			camera.stopPreview();
			preview.setCamera(null);
			camera.release();
			camera = null;
		}
		super.onPause();
	}

	private void showCamera(){
		SurfaceView sv = new SurfaceView(ctx);
		sv.setId(R.id.surfaceView);
		camera = Camera.open(cameraId);

		preview = new Preview(act, sv);
		preview.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT));

		preview.setCamera(camera);

		fl.addView(preview);
		fl.addView(sv);

		camera.startPreview();

	}

	private void stopCamera(){
		fl.removeView(view.findViewById(R.id.surfaceView));

		camera.stopPreview();
		releaseCameraAndPreview();
	}

	public void toggleFlash() {
		if (flashOn) {
			// turn off flash
			flashBtn.setImageResource(R.drawable.ic_flash_off_black_24dp);
		} else {
			flashBtn.setImageResource(R.drawable.ic_flash_on_black_24dp);
		}
		flashOn = !flashOn;
	}

	public void toggleCamera() {
		if (cameraId == REAR) {
			cameraId = FRONT;
			rotateBtn.setImageResource(R.drawable.ic_camera_rear_black_24dp);
		} else {
			cameraId = REAR;
			rotateBtn.setImageResource(R.drawable.ic_camera_front_black_24dp);
		}
		stopCamera();
		showCamera();
		bringToFront();
	}

	private void releaseCameraAndPreview() {
		if (camera != null) {
			camera.release();
			camera = null;
		}
	}

	public void getImageFromGallery(View v){
		Intent gallery = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(gallery, IMAGE_LOADED);
	}


	private Bitmap getBitmapFromUri(Uri uri) throws IOException {
		ParcelFileDescriptor parcelFileDescriptor =
				ctx.getContentResolver().openFileDescriptor(uri, "r");
		FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
		Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
		parcelFileDescriptor.close();
		return image;
	}


	private void resetCam() {
		camera.startPreview();
		preview.setCamera(camera);
	}

	public void initViews(){
		//imgTest = (ImageView) findViewById(R.id.img_cam);
	}

	public void bringToFront(){

		for (View v : views) {
			v.bringToFront();
		}
	}

	public Bitmap RotateBitmap(Bitmap source, float angle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}

	Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
		public void onPictureTaken(final byte[] data, Camera camera) {
			new Thread(new Runnable() {
				@Override
				public void run() {

					//Bitmap b = RotateBitmap(Image.toBitmap(data), 0);

					String date = Helpers.getDate();
					String sender = Profile.getInstance().getSelf().getName();
					ServiceCommunicator.getInstance().forwardMessage(new Image(sender, date, data));
				}
			}).start();
		}
	};

	private View.OnTouchListener focusTintChanger = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch(event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					((ImageView)v).setColorFilter(Color.rgb(200, 200, 200));
					break;
				case MotionEvent.ACTION_UP:
					((ImageView)v).setColorFilter(Color.rgb(255, 255, 255));
					if (v.hasOnClickListeners()) {
						v.callOnClick();
					}
					break;
			}
			return true;
		}
	};
}
