package hussam_haroon.fotocon.activities.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.LinearLayout;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DateFormat;

import hussam_haroon.fotocon.R;
import hussam_haroon.fotocon.shared.Profile;

/**
 * Created by M_har on 30-Apr-17.
 */

public class Helpers {


	public static String getDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("h:mm a");
		return sdf.format(new Date());
	}

	public static void applyWallpaper(View v) {
		Drawable d = Profile.getInstance().getDrawableWallpaper();
		if (d != null) {
			v.setBackground(d);
		}
	}
}
