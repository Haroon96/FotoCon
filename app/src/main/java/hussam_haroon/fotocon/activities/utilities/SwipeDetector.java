package hussam_haroon.fotocon.activities.utilities;

import android.view.MotionEvent;

/**
 * Created by M_har on 15-Apr-17.
 */

public class SwipeDetector {
	private static float x1,x2;
	public static int LEFT_SWIPE = 1;
	public static int RIGHT_SWIPE = 2;
	private static final int MIN_DISTANCE = 150;

	public static int detect(MotionEvent event) {
		switch(event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				x1 = event.getX();
				break;
			case MotionEvent.ACTION_UP:
				x2 = event.getX();
				float deltaX = x2 - x1;
				if (Math.abs(deltaX) > MIN_DISTANCE) {
					if (x2 > x1) {
						return LEFT_SWIPE;
					}
					else {
						return RIGHT_SWIPE;
					}
				}
				break;
		}
		return 0;
	}
}
